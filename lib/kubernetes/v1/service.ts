import * as pulumi from "@pulumi/pulumi";
import * as k8s from "@pulumi/kubernetes";

export interface ServiceArgs {
  metadata: pulumi.Input<k8s.types.input.meta.v1.ObjectMeta>;
  spec: pulumi.Input<k8s.types.input.core.v1.ServiceSpec>;
  hostnames?: pulumi.Input<pulumi.Input<string>[]>;
  target?: pulumi.Input<string>;
}

export class Service extends k8s.core.v1.Service {
  public readonly hostnames?: pulumi.Output<pulumi.Output<string>[]>;

  /**
   * Create a new service resource.
   * @param name The name of the service resource.
   * @param args A bag of arguments to control the service creation.
   * @param opts A bag of options that control this resource's behavior.
   */
  constructor(name: string, args: ServiceArgs, opts?: pulumi.CustomResourceOptions) {
    const serviceArgs = pulumi
      .all([args.metadata, args.spec, args.hostnames, args.target])
      .apply(([metadataWrapped, specWrapped, hostnames, target]) =>
        pulumi.all(hostnames ?? []).apply((hostnames) => {
          const metadata = metadataWrapped as k8s.types.input.meta.v1.ObjectMeta;
          const spec = specWrapped as k8s.types.input.core.v1.ServiceSpec;

          if (metadata.annotations === undefined) {
            metadata.annotations = {};
          }

          const annotations: { [key: string]: pulumi.Input<string> } = {};

          if (hostnames !== undefined && hostnames.length > 0) {
            annotations["external-dns.alpha.kubernetes.io/hostname"] = hostnames.join(",");
          }

          if (target !== undefined) {
            annotations["external-dns.alpha.kubernetes.io/target"] = target;
          }

          metadata.annotations = { ...annotations, ...metadata.annotations };

          spec.ipFamilyPolicy = "PreferDualStack";

          return { metadata, spec };
        }),
      );

    super(name, serviceArgs, opts);

    if (args.hostnames !== undefined) {
      this.hostnames = pulumi
        .all([args.hostnames])
        .apply(([hostnames]) => hostnames.map((hostname) => pulumi.output(hostname)));
    }
  }
}
