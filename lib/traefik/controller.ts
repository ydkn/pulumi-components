import * as pulumi from "@pulumi/pulumi";
import * as k8s from "@pulumi/kubernetes";
import * as fs from "fs";
import * as path from "path";
import * as url from "url";
import { ComponentResource } from "../common/index.js";
import * as kubernetes from "../kubernetes/index.js";
import { ServiceMonitor } from "../prometheus/index.js";
import { Dashboard } from "../grafana/index.js";

export interface TraefikArgs {
  namespace?: pulumi.Input<string>;
  replicas?: pulumi.Input<number>;
  serviceType?: pulumi.Input<string>;
  serviceLoadBalancerClass?: pulumi.Input<string>;
  serviceLabels?: pulumi.Input<{ [key: string]: pulumi.Input<string> }>;
  serviceAnnotations?: pulumi.Input<{ [key: string]: pulumi.Input<string> }>;
  ingressClass?: pulumi.Input<string>;
  defaultIngressClass?: pulumi.Input<boolean>;
  proxyProtocolIPs?: pulumi.Input<pulumi.Input<string>[]>;
}

export class Traefik extends ComponentResource {
  private readonly helmRelease: kubernetes.helm.v3.Release;
  private readonly metricsService: k8s.core.v1.Service;
  private readonly serviceMonitor: ServiceMonitor;
  private readonly grafanaDashboard: Dashboard;
  public readonly namespace: pulumi.Output<string>;
  public readonly ingressClassName: pulumi.Output<string>;

  /**
   * Create a new traefik ingress controller resource.
   * @param name The name of the traefik ingress controller resource.
   * @param args A bag of arguments to control the traefik ingress controller creation.
   * @param opts A bag of options that control this resource's behavior.
   */
  constructor(name: string, args: TraefikArgs, opts?: pulumi.ComponentResourceOptions) {
    super("traefik:controller", name, args, opts);

    opts = this.opts(k8s.Provider);

    if (args.replicas === undefined) {
      args.replicas = 1;
    }

    if (args.serviceType === undefined) {
      args.serviceType = "ClusterIP";
    }

    if (args.serviceLabels === undefined) {
      args.serviceLabels = {};
    }

    if (args.serviceAnnotations === undefined) {
      args.serviceAnnotations = {};
    }

    this.namespace = args.namespace
      ? pulumi.output(args.namespace)
      : new k8s.core.v1.Namespace(name, { metadata: { name } }, opts).metadata.name;

    this.ingressClassName = pulumi.output(args.ingressClass ?? "traefik");

    this.helmRelease = new kubernetes.helm.v3.Release(
      name,
      {
        name,
        namespace: this.namespace,
        repositoryOpts: { repo: "https://helm.traefik.io/traefik" },
        chart: "traefik",
        values: {
          fullnameOverride: name,
          deployment: {
            replicas: args.replicas,
            minReadySeconds: 10,
          },
          globalArguments: [],
          additionalArguments: pulumi.all([args.proxyProtocolIPs]).apply(([proxyProtocolIPs]) => {
            const args = [
              "--entrypoints.web.http.redirections.entryPoint.to=:443",
              "--entrypoints.web.http.redirections.entryPoint.scheme=https",
              "--entrypoints.web.http.redirections.entrypoint.permanent=true",
              "--serversTransport.insecureSkipVerify=true",
            ];

            if (proxyProtocolIPs !== undefined && proxyProtocolIPs.length > 0) {
              args.push(`--entrypoints.web.proxyprotocol.trustedips=${proxyProtocolIPs.join(",")}`);
              args.push(`--entrypoints.websecure.proxyprotocol.trustedips=${proxyProtocolIPs.join(",")}`);
            }

            return args;
          }),
          providers: {
            kubernetesCRD: {
              ingressClass: args.ingressClass,
            },
            kubernetesIngress: {
              ingressClass: args.ingressClass,
              publishedService: { enabled: true },
              allowExternalNameServices: true,
            },
          },
          ingressClass: {
            enabled: true,
            isDefaultClass: args.defaultIngressClass ?? false,
          },
          ingressRoute: { dashboard: { enabled: false } },
          logs: {
            general: { format: "json" },
            access: { enabled: false },
          },
          service: {
            type: args.serviceType,
            labels: args.serviceLabels,
            annotations: args.serviceAnnotations,
            ipFamilyPolicy: "PreferDualStack",
            ipFamilies: ["IPv4", "IPv6"],
            spec: {
              loadBalancerClass: args.serviceLoadBalancerClass,
            },
          },
          metrics: {
            prometheus: { entryPoint: "metrics", addRoutersLabels: true },
          },
          podDisruptionBudget: {
            enabled: true,
            maxUnavailable: "33%",
          },
          affinity: {
            podAntiAffinity: {
              preferredDuringSchedulingIgnoredDuringExecution: [
                {
                  weight: 100,
                  podAffinityTerm: {
                    labelSelector: {
                      matchExpressions: [
                        { key: "app.kubernetes.io/name", operator: "In", values: ["traefik"] },
                        {
                          key: "app.kubernetes.io/instance",
                          operator: "In",
                          values: [pulumi.interpolate`${name}-${this.namespace}`],
                        },
                      ],
                    },
                    topologyKey: "kubernetes.io/hostname",
                  },
                },
              ],
            },
          },
          resources: {
            requests: { memory: "128Mi" },
            limits: { memory: "192Mi" },
          },
        },
      },
      opts,
    );

    const metricsMetadata = {
      name: `${name}-metrics`,
      namespace: this.namespace,
      labels: {
        "app.kubernetes.io/name": "traefik",
        "app.kubernetes.io/instance": pulumi.interpolate`${name}-traefik`,
      },
    };

    this.metricsService = new kubernetes.v1.Service(
      `${name}-metrics`,
      {
        metadata: metricsMetadata,
        spec: {
          selector: metricsMetadata.labels,
          ports: [{ name: "metrics", port: 9100, targetPort: "metrics" }],
        },
      },
      { ...opts, dependsOn: [this.helmRelease] },
    );

    this.serviceMonitor = new ServiceMonitor(
      `${name}-metrics`,
      {
        metadata: metricsMetadata,
        spec: {
          namespaceSelector: { matchNames: [this.namespace] },
          selector: { matchLabels: metricsMetadata.labels },
          endpoints: [
            {
              port: "metrics",
              honorLabels: true,
              metricRelabelings: [
                {
                  action: "drop",
                  sourceLabels: ["__name__"],
                  regex: ["go_.*"].join("|"),
                },
              ],
            },
          ],
        },
      },
      { ...opts, dependsOn: [this.metricsService] },
    );

    this.grafanaDashboard = new Dashboard(
      `${name}-grafana-dashboard`,
      {
        metadata: { namespace: this.namespace, name: pulumi.interpolate`${name}-grafana-dashboard` },
        name: "traefik",
        data: fs.readFileSync(
          path.join(path.dirname(url.fileURLToPath(import.meta.url)), "assets", "dashboard.json"),
          "utf-8",
        ),
      },
      opts,
    );

    this.registerOutputs();
  }
}
