import * as pulumi from "@pulumi/pulumi";
import * as k8s from "@pulumi/kubernetes";

export const removeCrdDescriptions = (o: any, opts: pulumi.CustomResourceOptions): void => {
  if (o === undefined || o.apiVersion === undefined || o.kind === undefined) {
    return;
  }

  if (o.apiVersion !== "apiextensions.k8s.io/v1" || o.kind !== "CustomResourceDefinition") {
    return;
  }

  if (o.spec === undefined || o.spec.versions === undefined) {
    return;
  }

  o.spec.versions.forEach((v: k8s.types.input.apiextensions.v1.CustomResourceDefinitionVersion) => {
    if (v.schema === undefined) {
      return;
    }

    const schema = v.schema as k8s.types.input.apiextensions.v1.CustomResourceValidation;

    if (schema.openAPIV3Schema === undefined) {
      return;
    }

    removeDescription(schema.openAPIV3Schema);
  });
};

const removeDescription = (o?: pulumi.Input<k8s.types.input.apiextensions.v1.JSONSchemaProps>): void => {
  if (o === undefined) {
    return;
  }

  const schemaProps = o as k8s.types.input.apiextensions.v1.JSONSchemaProps;

  delete schemaProps.description;

  if (schemaProps.allOf !== undefined) {
    for (const p of schemaProps.allOf as pulumi.Input<k8s.types.input.apiextensions.v1.JSONSchemaProps>[]) {
      removeDescription(p);
    }
  }

  if (schemaProps.anyOf !== undefined) {
    for (const p of schemaProps.anyOf as pulumi.Input<k8s.types.input.apiextensions.v1.JSONSchemaProps>[]) {
      removeDescription(p);
    }
  }

  removeDescription(schemaProps.not);

  if (schemaProps.oneOf !== undefined) {
    for (const p of schemaProps.oneOf as pulumi.Input<k8s.types.input.apiextensions.v1.JSONSchemaProps>[]) {
      removeDescription(p);
    }
  }

  if (schemaProps.patternProperties !== undefined) {
    const patternProperties = schemaProps.patternProperties as {
      [key: string]: k8s.types.input.apiextensions.v1.JSONSchemaProps;
    };

    for (const p in patternProperties) {
      removeDescription(patternProperties[p]);
    }
  }

  if (schemaProps.properties !== undefined) {
    const properties = schemaProps.properties as { [key: string]: k8s.types.input.apiextensions.v1.JSONSchemaProps };

    for (const p in properties) {
      removeDescription(properties[p]);
    }
  }
};
