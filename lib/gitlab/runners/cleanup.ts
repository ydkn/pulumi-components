import * as pulumi from "@pulumi/pulumi";
import * as k8s from "@pulumi/kubernetes";
import { ComponentResource } from "../../common/index.js";
import * as kubernetes from "../../kubernetes/index.js";

export interface RunnerCleanupArgs {
  namespace?: pulumi.Input<string>;
  accessToken: pulumi.Input<string>;
  schedule?: pulumi.Input<string>;
}

export class RunnerCleanup extends ComponentResource {
  private readonly secret: kubernetes.v1.Secret;
  private readonly cronjob: k8s.batch.v1.CronJob;
  public readonly namespace: pulumi.Output<string>;

  /**
   * Create a new GitLab runner cleanup resource.
   * @param name The name of the GitLab runner cleanup resource.
   * @param args A bag of arguments to control the GitLab runner cleanup creation.
   * @param opts A bag of options that control this resource's behavior.
   */
  constructor(name: string, args: RunnerCleanupArgs, opts?: pulumi.ComponentResourceOptions) {
    super("gitlab:runners:cleanup", name, args, opts);

    opts = this.opts(k8s.Provider);

    if (args.schedule === undefined) {
      args.schedule = "0 1 * * *";
    }

    this.namespace = args.namespace
      ? pulumi.output(args.namespace)
      : new k8s.core.v1.Namespace(name, { metadata: { name } }, opts).metadata.name;

    const metadata = {
      name,
      namespace: this.namespace,
      labels: {
        "app.kubernetes.io/name": "gitlab-runners",
        "app.kubernetes.io/component": "cleanup",
        "app.kubernetes.io/instance": name,
      },
    };

    this.secret = new kubernetes.v1.Secret(
      name,
      {
        metadata,
        stringData: {
          token: args.accessToken,
        },
      },
      opts,
    );

    this.cronjob = new k8s.batch.v1.CronJob(
      name,
      {
        metadata,
        spec: {
          schedule: args.schedule,
          concurrencyPolicy: "Forbid",
          successfulJobsHistoryLimit: 0,
          failedJobsHistoryLimit: 1,
          jobTemplate: {
            metadata: { labels: metadata.labels },
            spec: {
              template: {
                spec: {
                  restartPolicy: "OnFailure",
                  containers: [
                    {
                      name: "gitlab-runner-cleanup",
                      image: "registry.gitlab.com/ydkn/gitlab-runner-cleanup:latest",
                      imagePullPolicy: "Always",
                      env: [
                        {
                          name: "TOKEN",
                          valueFrom: { secretKeyRef: { name: this.secret.metadata.name, key: "token" } },
                        },
                      ],
                      securityContext: {
                        runAsUser: 65532,
                        runAsGroup: 65532,
                        readOnlyRootFilesystem: true,
                      },
                      resources: {
                        requests: { memory: "32Mi" },
                        limits: { memory: "48Mi" },
                      },
                    },
                  ],
                },
              },
            },
          },
        },
      },
      opts,
    );

    this.registerOutputs();
  }
}
