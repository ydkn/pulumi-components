import * as pulumi from "@pulumi/pulumi";
import { Node, filterNodes } from "./node.js";

export const gatewayLabel = "ydkn.network/gateway";
export const gatewayLabelValue = "true";
export const gatewayNodeSelector = { "ydkn.network/gateway": "true" };

export function gatewayNodes(nodes: pulumi.Input<pulumi.Input<Node>[]>): pulumi.Output<pulumi.Output<Node>[]> {
  return filterNodes(nodes, gatewayNodeSelector);
}
