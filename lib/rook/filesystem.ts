import * as pulumi from "@pulumi/pulumi";
import * as k8s from "@pulumi/kubernetes";
import { ComponentResource } from "../common/index.js";
import { BlockPoolSpecArgs } from "./blockPool.js";

export interface FilesystemDataPoolArgs extends BlockPoolSpecArgs {
  name: pulumi.Input<string>;
}

export interface FilesystemArgs {
  metadata: pulumi.Input<k8s.types.input.meta.v1.ObjectMeta>;
  dataPools: pulumi.Input<pulumi.Input<FilesystemDataPoolArgs>[]>;
  clusterId: pulumi.Input<string>;
  defaultStorageClass?: pulumi.Input<boolean>;
  preserveFilesystemOnDelete?: pulumi.Input<boolean>;
}

export class Filesystem extends ComponentResource {
  private readonly filesystem: k8s.apiextensions.CustomResource;

  /**
   * Create a new ceph filesystem resource.
   * @param name The name of the ceph filesystem resource.
   * @param args A bag of arguments to control the ceph filesystem creation.
   * @param opts A bag of options that control this resource's behavior.
   */
  constructor(name: string, args: FilesystemArgs, opts?: pulumi.ComponentResourceOptions) {
    super("rook:ceph:filesystem", name, args, opts);

    opts = this.opts(k8s.Provider);

    this.filesystem = new k8s.apiextensions.CustomResource(
      name,
      {
        apiVersion: "ceph.rook.io/v1",
        kind: "CephFilesystem",
        metadata: args.metadata,
        spec: {
          preserveFilesystemOnDelete: args.preserveFilesystemOnDelete ?? false,
          metadataServer: {
            activeCount: 1,
            activeStandby: true,
            resources: {
              requests: { cpu: "500m", memory: "768Mi" },
              limits: { cpu: "4000m", memory: "1536Mi" },
            },
            priorityClassName: "system-cluster-critical",
            placement: {
              podAntiAffinity: {
                preferredDuringSchedulingIgnoredDuringExecution: [
                  {
                    weight: 100,
                    podAffinityTerm: {
                      labelSelector: {
                        matchExpressions: [
                          {
                            key: "app.kubernetes.io/component",
                            operator: "In",
                            values: ["cephfilesystems.ceph.rook.io"],
                          },
                          {
                            key: "app.kubernetes.io/part-of",
                            operator: "In",
                            values: [name],
                          },
                        ],
                      },
                      topologyKey: "kubernetes.io/hostname",
                    },
                  },
                ],
              },
            },
          },
          metadataPool: {
            replicated: { size: 3 },
          },
          dataPools: pulumi.all([args.dataPools]).apply(([dataPools]) => {
            return dataPools.map((dataPool) =>
              pulumi.all([dataPool]).apply(([dataPool]) => {
                return {
                  name: dataPool.name,
                  failureDomain: dataPool.failureDomain ?? "host",
                  replicated: dataPool.replicated,
                  erasureCoded: dataPool.erasureCoded,
                };
              }),
            );
          }),
        },
      },
      opts,
    );

    pulumi
      .all([args.metadata, args.defaultStorageClass, args.dataPools])
      .apply(([metadata, defaultStorageClass, dataPools]) => {
        dataPools.forEach((dataPool) =>
          pulumi.all([dataPool]).apply(([dataPool]) => {
            return new k8s.storage.v1.StorageClass(
              `${name}-${dataPool.name}`,
              {
                metadata: {
                  ...metadata,
                  name: `${metadata.name}-${dataPool.name}`,
                  annotations: {
                    ...metadata.annotations,
                    "storageclass.kubernetes.io/is-default-class": defaultStorageClass ? "true" : "false",
                  },
                },
                provisioner: pulumi.interpolate`${args.clusterId}.cephfs.csi.ceph.com`,
                parameters: {
                  fsName: pulumi.all([args.metadata]).apply(([metadata]) => metadata.name!),
                  pool: pulumi
                    .all([args.metadata])
                    .apply(([metadata]) => pulumi.interpolate`${metadata.name}-${dataPool.name}`),
                  clusterID: args.clusterId,
                  "csi.storage.k8s.io/provisioner-secret-name": "rook-csi-cephfs-provisioner",
                  "csi.storage.k8s.io/provisioner-secret-namespace": this.filesystem.metadata.namespace,
                  "csi.storage.k8s.io/controller-expand-secret-name": "rook-csi-cephfs-provisioner",
                  "csi.storage.k8s.io/controller-expand-secret-namespace": this.filesystem.metadata.namespace,
                  "csi.storage.k8s.io/node-stage-secret-name": "rook-csi-cephfs-node",
                  "csi.storage.k8s.io/node-stage-secret-namespace": this.filesystem.metadata.namespace,
                  "csi.storage.k8s.io/fstype": "ext4",
                },
                reclaimPolicy: "Delete",
                allowVolumeExpansion: true,
                mountOptions: [],
              },
              opts,
            );
          }),
        );
      });

    this.registerOutputs();
  }
}
