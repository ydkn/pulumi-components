import * as pulumi from "@pulumi/pulumi";
import * as aws from "@pulumi/aws";
import { ComponentResource } from "../common/index.js";

export interface AccessKeyArgs {
  name?: pulumi.Input<string>;
}

export class AccessKey extends ComponentResource {
  public readonly user: aws.iam.User;
  public readonly accessKey: aws.iam.AccessKey;

  /**
   * Create a new AWS access key resource.
   * @param name The name of the AWS access key resource.
   * @param args A bag of arguments to control the AWS access key creation.
   * @param opts A bag of options that control this resource's behavior.
   */
  constructor(name: string, args: AccessKeyArgs, opts?: pulumi.ComponentResourceOptions) {
    super("aws:accessKey", name, {}, opts);

    opts = this.opts(aws.Provider);

    this.user = new aws.iam.User(name, { name: args.name ?? name }, opts);
    this.accessKey = this.newAccessKey(name, new Date(), opts);

    this.registerOutputs();
  }

  private newAccessKey(name: string, date: Date, opts?: pulumi.ComponentResourceOptions): aws.iam.AccessKey {
    return new aws.iam.AccessKey(
      `${name}-${date.getFullYear()}-${date.getMonth() + 1}`,
      { user: this.user.name },
      { ...opts, dependsOn: this.user },
    );
  }
}
