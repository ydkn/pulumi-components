import * as pulumi from "@pulumi/pulumi";
import * as random from "@pulumi/random";
import * as k8s from "@pulumi/kubernetes";
import * as path from "path";
import * as fs from "fs";
import * as url from "url";
import * as http from "http";
import * as https from "https";
import { ComponentResource } from "../common/index.js";
import * as kubernetes from "../kubernetes/index.js";
import { ServiceMonitor } from "../prometheus/index.js";
import { Dashboard } from "../grafana/index.js";

export interface MetricsArgs {
  username: pulumi.Input<string>;
  password: pulumi.Input<string>;
}

export interface NetworkServerArgs {
  namespace?: pulumi.Input<string>;
  ingressClassName?: pulumi.Input<string>;
  hostname: pulumi.Input<string>;
  informHostname: pulumi.Input<string>;
  storageClassName?: pulumi.Input<string>;
  storage?: pulumi.Input<string>;
  serviceType?: pulumi.Input<string>;
  serviceLabels?: pulumi.Input<{ [key: string]: pulumi.Input<string> }>;
  serviceLoadBalancerClass?: pulumi.Input<string>;
  metrics?: pulumi.Input<MetricsArgs>;
  protect?: boolean;
}

export class NetworkServer extends ComponentResource {
  private static readonly dashboards = [
    "UniFi-Poller_ Client DPI - Prometheus",
    "UniFi-Poller_ Client Insights - Prometheus",
    "UniFi-Poller_ Network Sites - Prometheus",
    "UniFi-Poller_ UAP Insights - Prometheus",
    "UniFi-Poller_ USG Insights - Prometheus",
    "UniFi-Poller_ USW Insights - Prometheus",
  ];

  private readonly mongodbInitUsername: pulumi.Output<string>;
  private readonly mongodbInitPassword: pulumi.Output<string>;
  private readonly mongodbUsername: pulumi.Output<string>;
  private readonly mongodbPassword: pulumi.Output<string>;
  private readonly mongodbAuthSource: pulumi.Output<string>;
  private readonly mongodbDatabase: pulumi.Output<string>;
  private readonly metricsSecret: k8s.core.v1.Secret;
  private readonly mongodb: k8s.core.v1.Secret;
  private readonly mongodbInitConfigMap: k8s.core.v1.ConfigMap;
  private readonly volumeClaim: k8s.core.v1.PersistentVolumeClaim;
  private readonly deployment: k8s.apps.v1.Deployment;
  private readonly podDisruptionBudget: k8s.policy.v1.PodDisruptionBudget;
  private readonly service: kubernetes.v1.Service;
  private readonly metricsService: kubernetes.v1.Service;
  private readonly serviceMonitor: ServiceMonitor;
  private readonly ingress: kubernetes.v1.SecureIngress;
  public readonly namespace: pulumi.Output<string>;
  public readonly informUrl: pulumi.Output<string>;

  /**
   * Create a new UniFi network server resource.
   * @param name The name of the UniFi network server resource.
   * @param args A bag of arguments to control the UniFi network server creation.
   * @param opts A bag of options that control this resource's behavior.
   */
  constructor(name: string, args: NetworkServerArgs, opts?: pulumi.ComponentResourceOptions) {
    super("unifi:network-server", name, args, opts);

    const k8sOpts = this.opts(k8s.Provider);

    opts = this.opts();

    if (args.storage === undefined) {
      args.storage = "10Gi";
    }

    this.mongodbInitUsername = pulumi.output("root");
    this.mongodbInitPassword = new random.RandomPassword(
      `${name}-mongodb-admin-password`,
      { length: 24, special: false },
      opts,
    ).result;

    this.mongodbUsername = pulumi.output("unifi");
    this.mongodbPassword = new random.RandomPassword(
      `${name}-mongodb-password`,
      { length: 24, special: false },
      opts,
    ).result;

    this.mongodbAuthSource = pulumi.output("admin");
    this.mongodbDatabase = pulumi.output("unifi");

    this.namespace = args.namespace
      ? pulumi.output(args.namespace)
      : new k8s.core.v1.Namespace(name, { metadata: { name } }, k8sOpts).metadata.name;

    const metadata = {
      name: `${name}-network-server`,
      namespace: this.namespace,
      labels: {
        "app.kubernetes.io/name": "unifi",
        "app.kubernetes.io/component": "network-server",
        "app.kubernetes.io/instance": name,
      },
    };

    this.metricsSecret = new kubernetes.v1.Secret(
      `${name}-metrics`,
      {
        metadata: { ...metadata, name: `${metadata.name}-metrics` },
        stringData: pulumi.all([args.metrics ?? ({} as MetricsArgs)]).apply(([metrics]) => {
          return { username: metrics.username, password: metrics.password };
        }),
      },
      k8sOpts,
    );

    this.mongodb = new kubernetes.v1.Secret(
      `${name}-mongodb`,
      {
        metadata: { ...metadata, name: `${metadata.name}-mongodb` },
        stringData: {
          INITDB_ROOT_USERNAME: this.mongodbInitUsername,
          INITDB_ROOT_PASSWORD: this.mongodbInitPassword,
          USERNAME: this.mongodbUsername,
          PASSWORD: this.mongodbPassword,
          DBNAME: this.mongodbDatabase,
          AUTHSOURCE: this.mongodbAuthSource,
        },
      },
      k8sOpts,
    );

    this.mongodbInitConfigMap = new k8s.core.v1.ConfigMap(
      `${name}-mongodb-init`,
      {
        metadata: { ...metadata, name: `${metadata.name}-mongodb-init` },
        data: {
          "init-mongo.sh": fs.readFileSync(
            path.join(path.dirname(url.fileURLToPath(import.meta.url)), "assets", "init-mongo.sh"),
            "utf-8",
          ),
        },
      },
      k8sOpts,
    );

    const volumeClaimName = `${name}-network-server-data`;

    this.volumeClaim = new k8s.core.v1.PersistentVolumeClaim(
      name,
      {
        metadata: { ...metadata, name: volumeClaimName },
        spec: {
          accessModes: ["ReadWriteOnce"],
          storageClassName: args.storageClassName,
          resources: { requests: { storage: args.storage } },
        },
      },
      { ...k8sOpts, protect: args.protect },
    );

    this.deployment = new k8s.apps.v1.Deployment(
      name,
      {
        metadata,
        spec: {
          selector: { matchLabels: metadata.labels },
          strategy: { type: "Recreate" },
          replicas: 1,
          template: {
            metadata: {
              labels: metadata.labels,
              annotations: {
                "backup.velero.io/backup-volumes": "backup",
                "pre.hook.backup.velero.io/command":
                  '["/bin/sh", "-c", "rm -rf /backup/* && cp -rp /config/data/backup/* /backup/"]',
                "pre.hook.backup.velero.io/container": "network-server",
                "pre.hook.backup.velero.io/timeout": "5m",
              },
            },
            spec: {
              containers: [
                {
                  name: "network-server",
                  image: "lscr.io/linuxserver/unifi-network-application:latest",
                  imagePullPolicy: "Always",
                  securityContext: { runAsNonRoot: false },
                  env: [
                    { name: "MEM_LIMIT", value: "860" },
                    { name: "MEM_STARTUP", value: "860" },
                    { name: "MONGO_HOST", value: "localhost" },
                    { name: "MONGO_PORT", value: "27017" },
                    { name: "MONGO_TLS", value: "false" },
                    {
                      name: "MONGO_USER",
                      valueFrom: { secretKeyRef: { name: this.mongodb.metadata.name, key: "USERNAME" } },
                    },
                    {
                      name: "MONGO_PASS",
                      valueFrom: { secretKeyRef: { name: this.mongodb.metadata.name, key: "PASSWORD" } },
                    },
                    {
                      name: "MONGO_DBNAME",
                      valueFrom: { secretKeyRef: { name: this.mongodb.metadata.name, key: "DBNAME" } },
                    },
                    {
                      name: "MONGO_AUTHSOURCE",
                      valueFrom: { secretKeyRef: { name: this.mongodb.metadata.name, key: "AUTHSOURCE" } },
                    },
                  ],
                  ports: [
                    { protocol: "UDP", containerPort: 3478, name: "p3478" },
                    { protocol: "UDP", containerPort: 5514, name: "p5514" },
                    { protocol: "TCP", containerPort: 6789, name: "p6789" },
                    { protocol: "TCP", containerPort: 8080, name: "p8080" },
                    { protocol: "TCP", containerPort: 8081, name: "p8081" },
                    { protocol: "TCP", containerPort: 8443, name: "p8443" },
                    { protocol: "TCP", containerPort: 8843, name: "p8843" },
                    { protocol: "TCP", containerPort: 8880, name: "p8880" },
                    { protocol: "UDP", containerPort: 10001, name: "p10001" },
                  ],
                  livenessProbe: {
                    httpGet: { path: "/", port: "p8443", scheme: "HTTPS" },
                    initialDelaySeconds: 60,
                    periodSeconds: 30,
                    failureThreshold: 6,
                  },
                  readinessProbe: {
                    httpGet: { path: "/", port: "p8443", scheme: "HTTPS" },
                    initialDelaySeconds: 60,
                    periodSeconds: 20,
                    failureThreshold: 3,
                  },
                  volumeMounts: [
                    { name: "data", mountPath: "/config/data", subPath: "unifi" },
                    { name: "backup", mountPath: "/backup" },
                  ],
                  resources: {
                    requests: { memory: "768Mi" },
                    limits: { memory: "1280Mi" },
                  },
                },
                {
                  name: "mongodb",
                  image: "docker.io/mongo:7.0",
                  imagePullPolicy: "Always",
                  envFrom: [{ secretRef: { name: this.mongodb.metadata.name }, prefix: "MONGO_" }],
                  ports: [{ protocol: "TCP", containerPort: 27017, name: "mongodb" }],
                  volumeMounts: [
                    {
                      name: "mongo-init",
                      mountPath: "/docker-entrypoint-initdb.d/init-mongo.sh",
                      subPath: "init-mongo.sh",
                      readOnly: true,
                    },
                    { name: "data", mountPath: "/data/db", subPath: "mongodb" },
                  ],
                  resources: {
                    requests: { memory: "192Mi" },
                    limits: { memory: "256Mi" },
                  },
                },
                {
                  name: "unpoller",
                  image: "ghcr.io/unpoller/unpoller:latest",
                  imagePullPolicy: "Always",
                  env: [
                    { name: "UP_INFLUXDB_DISABLE", value: "true" },
                    { name: "UP_POLLER_DEBUG", value: "false" },
                    { name: "UP_UNIFI_DYNAMIC", value: "false" },
                    { name: "UP_PROMETHEUS_HTTP_LISTEN", value: "0.0.0.0:9130" },
                    { name: "UP_PROMETHEUS_NAMESPACE", value: "unpoller" },
                    { name: "UP_UNIFI_CONTROLLER_0_URL", value: "https://localhost:8443" },
                    { name: "UP_UNIFI_CONTROLLER_0_SAVE_ALARMS", value: "true" },
                    { name: "UP_UNIFI_CONTROLLER_0_SAVE_ANOMALIES", value: "true" },
                    { name: "UP_UNIFI_CONTROLLER_0_SAVE_DPI", value: "true" },
                    { name: "UP_UNIFI_CONTROLLER_0_SAVE_EVENTS", value: "true" },
                    { name: "UP_UNIFI_CONTROLLER_0_SAVE_IDS", value: "true" },
                    { name: "UP_UNIFI_CONTROLLER_0_SAVE_SITES", value: "true" },
                    { name: "UP_UNIFI_CONTROLLER_0_SITES_0", value: "ALL" },
                    { name: "UP_UNIFI_CONTROLLER_0_VERIFY_SSL", value: "false" },
                    {
                      name: "UP_UNIFI_CONTROLLER_0_USER",
                      valueFrom: { secretKeyRef: { name: this.metricsSecret.metadata.name, key: "username" } },
                    },
                    {
                      name: "UP_UNIFI_CONTROLLER_0_PASS",
                      valueFrom: { secretKeyRef: { name: this.metricsSecret.metadata.name, key: "password" } },
                    },
                  ],
                  ports: [{ protocol: "TCP", containerPort: 9130, name: "http-metrics" }],
                  resources: {
                    requests: { memory: "64Mi" },
                    limits: { memory: "96Mi" },
                  },
                },
              ],
              volumes: [
                { name: "mongo-init", configMap: { name: this.mongodbInitConfigMap.metadata.name } },
                { name: "data", persistentVolumeClaim: { claimName: volumeClaimName } },
                { name: "backup", emptyDir: {} },
              ],
              affinity: {
                podAntiAffinity: {
                  preferredDuringSchedulingIgnoredDuringExecution: [
                    {
                      weight: 100,
                      podAffinityTerm: {
                        labelSelector: {
                          matchExpressions: [
                            {
                              key: "app.kubernetes.io/name",
                              operator: "In",
                              values: [metadata.labels["app.kubernetes.io/name"]],
                            },
                            {
                              key: "app.kubernetes.io/component",
                              operator: "In",
                              values: [metadata.labels["app.kubernetes.io/component"]],
                            },
                            {
                              key: "app.kubernetes.io/instance",
                              operator: "In",
                              values: [metadata.labels["app.kubernetes.io/instance"]],
                            },
                          ],
                        },
                        topologyKey: "kubernetes.io/hostname",
                      },
                    },
                  ],
                },
              },
            },
          },
        },
      },
      k8sOpts,
    );

    this.podDisruptionBudget = new k8s.policy.v1.PodDisruptionBudget(
      name,
      {
        metadata,
        spec: {
          minAvailable: 1,
          selector: { matchLabels: metadata.labels },
        },
      },
      k8sOpts,
    );

    this.service = new kubernetes.v1.Service(
      name,
      {
        hostnames: [args.informHostname],
        metadata: {
          ...metadata,
          labels: pulumi.all([args.serviceLabels]).apply(([labels]) => {
            return { ...metadata.labels, ...labels };
          }),
        },
        spec: {
          loadBalancerClass: args.serviceLoadBalancerClass,
          type: args.serviceType ?? "ClusterIP",
          selector: metadata.labels,
          ports: [
            { protocol: "UDP", port: 3478, name: "p3478" },
            { protocol: "UDP", port: 5514, name: "p5514" },
            { protocol: "TCP", port: 6789, name: "p6789" },
            { protocol: "TCP", port: 8080, name: "p8080" },
            { protocol: "TCP", port: 8081, name: "p8081" },
            { protocol: "TCP", port: 8443, name: "https" },
            { protocol: "TCP", port: 8843, name: "p8843" },
            { protocol: "TCP", port: 8880, name: "p8880" },
            { protocol: "UDP", port: 10001, name: "p10001" },
          ],
        },
      },
      { ...k8sOpts, dependsOn: [this.deployment] },
    );

    this.metricsService = new kubernetes.v1.Service(
      `${name}-metrics`,
      {
        metadata: { ...metadata, name: `${metadata.name}-metrics` },
        spec: {
          selector: metadata.labels,
          ports: [{ protocol: "TCP", port: 9130, name: "http-metrics" }],
        },
      },
      { ...k8sOpts, dependsOn: [this.deployment] },
    );

    this.serviceMonitor = new ServiceMonitor(
      `${name}-metrics`,
      {
        metadata,
        spec: {
          namespaceSelector: { matchNames: [this.namespace] },
          selector: { matchLabels: metadata.labels },
          endpoints: [{ port: "http-metrics", path: "/metrics", interval: "1m", scrapeTimeout: "10s" }],
        },
      },
      k8sOpts,
    );

    this.ingress = new kubernetes.v1.SecureIngress(
      name,
      {
        metadata,
        spec: {
          ingressClassName: args.ingressClassName,
          rules: [
            {
              host: args.hostname,
              http: {
                paths: [
                  {
                    path: "/",
                    pathType: "Prefix",
                    backend: { service: { name: this.service.metadata.name, port: { name: "https" } } },
                  },
                ],
              },
            },
          ],
        },
      },
      k8sOpts,
    );

    for (const id of NetworkServer.dashboards) {
      this.dashboard(name, this.namespace, id, k8sOpts);
    }

    this.informUrl = pulumi.concat("http://", args.informHostname, ":8080", "/inform");

    this.registerOutputs();
  }

  private dashboard(
    name: string,
    namespace: pulumi.Input<string>,
    id: string,
    opts: pulumi.ComponentResourceOptions,
  ): Dashboard {
    const sanitizedID = id
      .replaceAll("UniFi-Poller_ ", "")
      .replaceAll(" - Prometheus", "")
      .replaceAll(" ", "-")
      .toLowerCase();

    return new Dashboard(
      `${name}-grafana-dashboard-${sanitizedID}`,
      {
        metadata: { name: pulumi.interpolate`${name}-grafana-dashboard-${sanitizedID}`, namespace },
        name: sanitizedID,
        data: new Promise((resolve, reject) => {
          https
            .request(
              `https://raw.githubusercontent.com/unpoller/dashboards/refs/heads/master/v2.0.0/${encodeURI(id)}.json`,
              (r: http.IncomingMessage): void => {
                let data = "";

                r.on("data", (chunk: string): void => {
                  data += chunk;
                });
                r.on("end", (): void => {
                  resolve(data.replaceAll("${DS_PROMETHEUS}", "prometheus").replaceAll(" - Prometheus", ""));
                });
                r.on("error", (err): void => {
                  reject(err);
                });
              },
            )
            .end();
        }),
      },
      opts,
    );
  }
}
