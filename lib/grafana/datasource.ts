import * as pulumi from "@pulumi/pulumi";
import * as k8s from "@pulumi/kubernetes";
import * as yaml from "js-yaml";
import * as kubernetes from "../kubernetes/index.js";

export interface DataSourceArgs {
  metadata: pulumi.Input<k8s.types.input.meta.v1.ObjectMeta>;
  type: pulumi.Input<string>;
  name: pulumi.Input<string>;
  url: pulumi.Input<string>;
  orgId?: pulumi.Input<string>;
  isDefault?: pulumi.Input<boolean>;
}

export class DataSource extends kubernetes.v1.Secret {
  /**
   * Create a new Grafana data source resource.
   * @param name The name of the Grafana data source resource.
   * @param args A bag of arguments to control the Grafana data source creation.
   * @param opts A bag of options that control this resource's behavior.
   */
  constructor(name: string, args: DataSourceArgs, opts?: pulumi.CustomResourceOptions) {
    super(
      name,
      {
        metadata: pulumi.all([args.metadata]).apply(([metadata]) => {
          return {
            ...metadata,
            labels: { ...metadata.labels, grafana_datasource: "1" },
          };
        }),
        stringData: pulumi
          .all([args.type, args.name, args.url, args.orgId, args.isDefault])
          .apply(([dsType, dsName, url, orgId, isDefault]) => {
            const data: { [key: string]: string } = {};

            data[`${dsName}.yml`] = yaml.dump({
              apiVersion: 1,
              deleteDatasources: [{ name: dsName, orgId: orgId ?? 1 }],
              datasources: [
                {
                  uid: name,
                  name: dsName,
                  type: dsType,
                  access: "proxy",
                  orgId: orgId ?? 1,
                  url,
                  editable: false,
                  isDefault: isDefault ?? false,
                },
              ],
            });

            return data;
          }),
      },
      opts,
    );
  }
}
