import * as pulumi from "@pulumi/pulumi";
import * as random from "@pulumi/random";
import * as k8s from "@pulumi/kubernetes";
import * as http from "http";
import * as https from "https";
import { ComponentResource } from "../common/index.js";
import * as kubernetes from "../kubernetes/index.js";
import { Dashboard } from "./dashboard.js";

export interface OAuthArgs {
  clientId: pulumi.Input<string>;
  clientSecret?: pulumi.Input<string>;
  authUrl: pulumi.Input<string>;
  tokenUrl: pulumi.Input<string>;
  apiUrl: pulumi.Input<string>;
}

export interface GrafanaArgs {
  namespace?: pulumi.Input<string>;
  hostname?: pulumi.Input<string>;
  ingressClassName?: pulumi.Input<string>;
  oauth?: pulumi.Input<OAuthArgs>;
  replicas?: pulumi.Input<number>;
}

export class Grafana extends ComponentResource {
  private static readonly kubernetesDashboards = [
    "k8s-system-coredns",
    "k8s-views-global",
    "k8s-views-namespaces",
    "k8s-views-nodes",
    "k8s-views-pods",
  ];
  private static readonly cephDashboards = [
    "ceph-cluster",
    "ceph-cluster-advanced",
    "hosts-overview",
    "host-details",
    "osds-overview",
    "osd-device-details",
    "pool-overview",
    "pool-detail",
    "cephfs-overview",
  ];

  private readonly helmRelease: kubernetes.helm.v3.Release;
  public readonly adminPassword: pulumi.Output<string>;
  public readonly namespace: pulumi.Output<string>;

  /**
   * Create a new grafana resource.
   * @param name The name of grafana resource.
   * @param args A bag of arguments to control the grafana creation.
   * @param opts A bag of options that control this resource's behavior.
   */
  constructor(name: string, args: GrafanaArgs, opts?: pulumi.ComponentResourceOptions) {
    super("grafana", name, args, opts);

    const emptyOpts = this.opts();
    const k8sOpts = this.opts(k8s.Provider);

    if (args.replicas === undefined) {
      args.replicas = 1;
    }

    this.adminPassword = pulumi.secret(
      new random.RandomPassword(`${name}-admin-password`, { length: 24, special: true }, emptyOpts).result,
    );

    this.namespace = args.namespace
      ? pulumi.output(args.namespace)
      : new k8s.core.v1.Namespace(name, { metadata: { name } }, k8sOpts).metadata.name;

    this.helmRelease = new kubernetes.helm.v3.Release(
      name,
      {
        name,
        namespace: this.namespace,
        repositoryOpts: { repo: "https://grafana.github.io/helm-charts" },
        chart: "grafana",
        values: {
          fullnameOverride: name,
          replicas: args.replicas,
          adminPassword: this.adminPassword,
          testFramework: { enabled: false },
          plugins: ["grafana-clock-panel", "grafana-piechart-panel", "natel-discrete-panel", "vonage-status-panel"],
          ingress: kubernetes.helm.v3.ingressValues(name, [args.hostname], args.ingressClassName),
          serviceAccount: { create: true, autoMount: true },
          rbac: { pspEnabled: false },
          serviceMonitor: { enabled: true },
          "grafana.ini": {
            server: {
              root_url: pulumi.interpolate`https://${args.hostname}`,
            },
            "auth.generic_oauth": pulumi.all([args.oauth]).apply(([oauth]) => {
              if (oauth === undefined) {
                return { enabled: false };
              }

              return {
                enabled: true,
                client_id: oauth.clientId,
                client_secret: oauth.clientSecret,
                scopes: "openid email profile offline_access roles",
                auth_url: oauth.authUrl,
                token_url: oauth.tokenUrl,
                api_url: oauth.apiUrl,
                use_pkce: true,
                auto_login: true,
                use_refresh_token: true,
                email_attribute_path: "email",
                login_attribute_path: "preferred_username",
                name_attribute_path: "name",
                role_attribute_path: [
                  `"urn:zitadel:iam:org:project:roles".GrafanaAdmin && 'GrafanaAdmin'`,
                  `"urn:zitadel:iam:org:project:roles".Admin && 'Admin'`,
                  `"urn:zitadel:iam:org:project:roles".Editor && 'Editor'`,
                  `"urn:zitadel:iam:org:project:roles".Viewer && 'Viewer'`,
                ].join(" || "),
                role_attribute_strict: true,
                allow_assign_grafana_admin: true,
              };
            }),
            analytics: { check_for_updates: false },
          },
          sidecar: {
            datasources: { enabled: true, searchNamespace: "ALL" },
            dashboards: { enabled: true, searchNamespace: "ALL" },
            resources: {
              requests: { memory: "96Mi" },
              limits: { memory: "128Mi" },
            },
          },
          resources: {
            requests: { memory: "256Mi" },
            limits: { memory: "384Mi" },
          },
        },
      },
      k8sOpts,
    );

    for (const id of Grafana.kubernetesDashboards) {
      this.kubernetesDashboard(name, this.namespace, id, k8sOpts);
    }

    for (const id of Grafana.cephDashboards) {
      this.cephDashboard(name, this.namespace, id, k8sOpts);
    }

    this.registerOutputs();
  }

  private dashboard(
    name: string,
    namespace: pulumi.Input<string>,
    id: string,
    url: string,
    opts: pulumi.ComponentResourceOptions,
  ): Dashboard {
    return new Dashboard(
      `${name}-grafana-dashboard-${id}`,
      {
        metadata: { name: pulumi.interpolate`${name}-grafana-dashboard-${id}`, namespace },
        name: id,
        data: new Promise((resolve, reject) => {
          https
            .request(url, (r: http.IncomingMessage): void => {
              let data = "";

              r.on("data", (chunk: string): void => {
                data += chunk;
              });
              r.on("end", (): void => {
                resolve(data);
              });
              r.on("error", (err): void => {
                reject(err);
              });
            })
            .end();
        }),
      },
      opts,
    );
  }

  private kubernetesDashboard(
    name: string,
    namespace: pulumi.Input<string>,
    id: string,
    opts: pulumi.ComponentResourceOptions,
  ): Dashboard {
    return this.dashboard(
      name,
      namespace,
      id,
      `https://raw.githubusercontent.com/dotdc/grafana-dashboards-kubernetes/master/dashboards/${id}.json`,
      opts,
    );
  }

  private cephDashboard(
    name: string,
    namespace: pulumi.Input<string>,
    id: string,
    opts: pulumi.ComponentResourceOptions,
  ): Dashboard {
    return this.dashboard(
      name,
      namespace,
      id,
      `https://raw.githubusercontent.com/ceph/ceph/refs/heads/main/monitoring/ceph-mixin/dashboards_out/${id}.json`,
      opts,
    );
  }
}
