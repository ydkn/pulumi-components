import * as pulumi from "@pulumi/pulumi";
import * as k8s from "@pulumi/kubernetes";
import { ComponentResource } from "../common/index.js";
import * as kubernetes from "../kubernetes/index.js";

export interface LocalPathProvisionerArgs {
  namespace?: pulumi.Input<string>;
  storagePath: pulumi.Input<string>;
  replicas?: pulumi.Input<number>;
  defaultClass?: pulumi.Input<boolean>;
  nodeSelector?: pulumi.Input<{ [key: string]: pulumi.Input<string> }>;
}

export class LocalPathProvisioner extends ComponentResource {
  public static readonly storageClassName = "local-path";

  private readonly helmRelease: kubernetes.helm.v3.Release;
  public readonly namespace: pulumi.Output<string>;

  /**
   * Create a new local path provisioner resource.
   * @param name The name of the local path provisioner resource.
   * @param args A bag of arguments to control the local path provisioner creation.
   * @param opts A bag of options that control this resource's behavior.
   */
  constructor(name: string, args: LocalPathProvisionerArgs, opts?: pulumi.ComponentResourceOptions) {
    super("local-path-provisioner", name, args, opts);

    opts = this.opts(k8s.Provider);

    if (args.replicas === undefined) {
      args.replicas = 1;
    }

    this.namespace = args.namespace
      ? pulumi.output(args.namespace)
      : new k8s.core.v1.Namespace(name, { metadata: { name } }, opts).metadata.name;

    this.helmRelease = new kubernetes.helm.v3.Release(
      name,
      {
        name,
        namespace: this.namespace,
        repositoryOpts: { repo: "https://charts.containeroo.ch" },
        chart: "local-path-provisioner",
        values: {
          fullnameOverride: name,
          replicaCount: args.replicas,
          storageClass: {
            name: LocalPathProvisioner.storageClassName,
            defaultClass: args.defaultClass ?? false,
            pathPattern: "{{ .PVName }}",
          },
          nodePathMap: [
            {
              node: "DEFAULT_PATH_FOR_NON_LISTED_NODES",
              paths: [args.storagePath],
            },
          ],
          nodeSelector: args.nodeSelector,
          tolerations: [
            { key: "node-role.kubernetes.io/master", operator: "Exists", effect: "NoSchedule" },
            { key: "node-role.kubernetes.io/control-plane", operator: "Exists", effect: "NoSchedule" },
          ],
          affinity: {
            podAntiAffinity: {
              preferredDuringSchedulingIgnoredDuringExecution: [
                {
                  weight: 100,
                  podAffinityTerm: {
                    labelSelector: {
                      matchExpressions: [
                        { key: "app.kubernetes.io/name", operator: "In", values: [name] },
                        { key: "app.kubernetes.io/instance", operator: "In", values: [name] },
                      ],
                    },
                    topologyKey: "kubernetes.io/hostname",
                  },
                },
              ],
            },
          },
          resources: {
            requests: { memory: "64Mi" },
            limits: { memory: "96Mi" },
          },
        },
      },
      opts,
    );

    this.registerOutputs();
  }
}
