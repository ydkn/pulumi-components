import * as pulumi from "@pulumi/pulumi";
import * as k8s from "@pulumi/kubernetes";
import * as fs from "fs";
import * as path from "path";
import * as url from "url";
import { ComponentResource } from "../common/index.js";
import * as kubernetes from "../kubernetes/index.js";
import { Dashboard } from "../grafana/index.js";
import { ClusterIssuer } from "./issuer.js";
import { Certificate } from "./certificate.js";

export interface LetsEncryptCloudflareArgs {
  token: pulumi.Input<string>;
  email: pulumi.Input<string>;
}

export interface CertManagerArgs {
  namespace?: pulumi.Input<string>;
  replicas?: pulumi.Input<number>;
  letsencryptCloudflare?: pulumi.Input<LetsEncryptCloudflareArgs>;
}

export class CertManager extends ComponentResource {
  private readonly helmRelease: kubernetes.helm.v3.Release;
  private readonly grafanaDashboard: Dashboard;
  public readonly namespace: pulumi.Output<string>;
  public readonly clusterIssuers: pulumi.Output<{ [key: string]: pulumi.Output<string> }>;

  /**
   * Create a new cert-manager resource.
   * @param name The name of cert-manager resource.
   * @param args A bag of arguments to control the cert-manager creation.
   * @param opts A bag of options that control this resource's behavior.
   */
  constructor(name: string, args: CertManagerArgs, opts?: pulumi.ComponentResourceOptions) {
    super("cert-manager", name, args, opts);

    opts = this.opts(k8s.Provider);

    if (args.replicas === undefined) {
      args.replicas = 1;
    }

    this.namespace = args.namespace
      ? pulumi.output(args.namespace)
      : new k8s.core.v1.Namespace(name, { metadata: { name } }, opts).metadata.name;

    const metadata = {
      name,
      namespace: this.namespace,
      labels: {
        "app.kubernetes.io/name": "cert-manager",
        "app.kubernetes.io/component": "cert-manager",
        "app.kubernetes.io/instance": name,
      },
    };

    this.helmRelease = new kubernetes.helm.v3.Release(
      name,
      {
        name: metadata.name,
        namespace: metadata.namespace,
        repositoryOpts: { repo: "https://charts.jetstack.io" },
        chart: "cert-manager",
        values: {
          fullnameOverride: name,
          replicaCount: args.replicas,
          installCRDs: true,
          global: {
            leaderElection: { namespace: metadata.namespace },
          },
          extraArgs: ["--enable-certificate-owner-ref=true"],
          webhook: {
            replicaCount: args.replicas,
            affinity: {
              podAntiAffinity: {
                preferredDuringSchedulingIgnoredDuringExecution: [
                  {
                    weight: 100,
                    podAffinityTerm: {
                      labelSelector: {
                        matchExpressions: [
                          { key: "app.kubernetes.io/name", operator: "In", values: ["cert-manager"] },
                          { key: "app.kubernetes.io/component", operator: "In", values: ["webhook"] },
                          { key: "app.kubernetes.io/instance", operator: "In", values: [name] },
                        ],
                      },
                      topologyKey: "kubernetes.io/hostname",
                    },
                  },
                ],
              },
            },
            serviceIPFamilyPolicy: "PreferDualStack",
            resources: {
              requests: { memory: "48Mi" },
              limits: { memory: "96Mi" },
            },
          },
          cainjector: {
            replicaCount: args.replicas,
            affinity: {
              podAntiAffinity: {
                preferredDuringSchedulingIgnoredDuringExecution: [
                  {
                    weight: 100,
                    podAffinityTerm: {
                      labelSelector: {
                        matchExpressions: [
                          { key: "app.kubernetes.io/name", operator: "In", values: ["cert-manager"] },
                          { key: "app.kubernetes.io/component", operator: "In", values: ["cainjector"] },
                          { key: "app.kubernetes.io/instance", operator: "In", values: [name] },
                        ],
                      },
                      topologyKey: "kubernetes.io/hostname",
                    },
                  },
                ],
              },
            },
            resources: {
              requests: { memory: "128Mi" },
              limits: { memory: "384Mi" },
            },
          },
          prometheus: { servicemonitor: { enabled: true } },
          affinity: {
            podAntiAffinity: {
              preferredDuringSchedulingIgnoredDuringExecution: [
                {
                  weight: 100,
                  podAffinityTerm: {
                    labelSelector: {
                      matchExpressions: [
                        { key: "app.kubernetes.io/name", operator: "In", values: ["cert-manager"] },
                        { key: "app.kubernetes.io/component", operator: "In", values: ["controller"] },
                        { key: "app.kubernetes.io/instance", operator: "In", values: [name] },
                      ],
                    },
                    topologyKey: "kubernetes.io/hostname",
                  },
                },
              ],
            },
          },
          serviceIPFamilyPolicy: "PreferDualStack",
          resources: {
            requests: { memory: "128Mi" },
            limits: { memory: "384Mi" },
          },
        },
      },
      opts,
    );

    this.clusterIssuers = pulumi.all([args.letsencryptCloudflare]).apply(([letsencryptCloudflare]) => {
      const clusterIssuers: { [key: string]: pulumi.Output<string> } = {};

      clusterIssuers.selfSigned = this.selfSignedIssuer(name, metadata, opts).metadata.name;

      if (letsencryptCloudflare !== undefined) {
        clusterIssuers.letsencrypt = this.letsencryptCloudflareIssuer(
          name,
          metadata,
          letsencryptCloudflare,
          opts,
        ).metadata.name;
      }

      return clusterIssuers;
    });

    this.grafanaDashboard = new Dashboard(
      `${name}-grafana-dashboard`,
      {
        metadata: { ...metadata, name: pulumi.interpolate`${name}-grafana-dashboard` },
        name: "cert-manager",
        data: fs.readFileSync(
          path.join(path.dirname(url.fileURLToPath(import.meta.url)), "assets", "dashboard.json"),
          "utf-8",
        ),
      },
      opts,
    );

    this.registerOutputs();
  }

  private selfSignedIssuer(
    name: string,
    metadata: k8s.types.input.meta.v1.ObjectMeta,
    opts?: pulumi.ComponentResourceOptions,
  ): pulumi.Output<ClusterIssuer> {
    return pulumi.output(
      new ClusterIssuer(
        `${name}-self-signed`,
        {
          metadata: { ...metadata, name: "self-signed" },
          spec: { selfSigned: {} },
        },
        { ...opts, dependsOn: [this.helmRelease] },
      ),
    );
  }

  private letsencryptCloudflareIssuer(
    name: string,
    metadata: k8s.types.input.meta.v1.ObjectMeta,
    args: pulumi.Input<LetsEncryptCloudflareArgs>,
    opts?: pulumi.ComponentResourceOptions,
  ): pulumi.Output<ClusterIssuer> {
    return pulumi.all([args]).apply(([args]) => {
      const secret = new kubernetes.v1.Secret(
        `${name}-cloudflare`,
        {
          metadata: { ...metadata, name: "cloudflare" },
          stringData: {
            apiToken: args.token,
          },
        },
        opts,
      );

      return new ClusterIssuer(
        `${name}-cloudflare`,
        {
          metadata: { ...metadata, name: Certificate.DefaultClusterIssuer },
          spec: {
            acme: {
              email: args.email,
              server: "https://acme-v02.api.letsencrypt.org/directory",
              privateKeySecretRef: { name: Certificate.DefaultClusterIssuer },
              solvers: [
                {
                  dns01: {
                    cloudflare: {
                      email: args.email,
                      apiTokenSecretRef: { name: secret.metadata.name, key: "apiToken" },
                    },
                  },
                },
              ],
            },
          },
        },
        { ...opts, dependsOn: [this.helmRelease] },
      );
    });
  }
}
