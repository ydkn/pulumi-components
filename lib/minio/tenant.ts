import * as pulumi from "@pulumi/pulumi";
import * as random from "@pulumi/random";
import * as k8s from "@pulumi/kubernetes";
import { ComponentResource } from "../common/index.js";
import * as kubernetes from "../kubernetes/index.js";
import { Certificate } from "../certManager/index.js";

export interface PoolArgs {
  name: pulumi.Input<string>;
  servers: pulumi.Input<number>;
  volumesPerServer?: pulumi.Input<number>;
  size?: pulumi.Input<string>;
  storageClassName?: pulumi.Input<string>;
  resources?: pulumi.Input<k8s.types.input.core.v1.ResourceRequirements>;
}

export interface IngressArgs {
  enabled?: pulumi.Input<boolean>;
  ingressClassName?: pulumi.Input<string>;
  annotations?: pulumi.Input<{ [key: string]: pulumi.Input<string> }>;
  host: pulumi.Input<string>;
}

export interface BucketsIngressArgs {
  ingressClassName?: pulumi.Input<string>;
  annotations?: pulumi.Input<{ [key: string]: pulumi.Input<string> }>;
  hosts: pulumi.Input<pulumi.Input<string>[]>;
}

export interface BucketArgs {
  name: pulumi.Input<string>;
  objectLock?: pulumi.Input<boolean>;
}

export interface UserArgs {
  name: pulumi.Input<string>;
}

export interface TenantArgs {
  namespace?: pulumi.Input<string>;
  pools: pulumi.Input<pulumi.Input<PoolArgs>[]>;
  resources?: pulumi.Input<k8s.types.input.core.v1.ResourceRequirements>;
  apiIngress?: pulumi.Input<IngressArgs>;
  consoleIngress?: pulumi.Input<IngressArgs>;
  bucketsIngress?: pulumi.Input<BucketsIngressArgs>;
  standardStorageClass?: pulumi.Input<string>;
  reducedStorageClass?: pulumi.Input<string>;
  domains?: pulumi.Input<pulumi.Input<string>[]>;
  buckets?: pulumi.Input<pulumi.Input<BucketArgs>[]>;
  users?: pulumi.Input<pulumi.Input<UserArgs>[]>;
  region?: pulumi.Input<string>;
  protect?: boolean;
}

interface internalIngressArgs extends IngressArgs {
  tls: pulumi.Input<{ hosts: pulumi.Input<pulumi.Input<string>[]>; secretName: pulumi.Input<string> }[]>;
}

export class Tenant extends ComponentResource {
  private readonly helmRelease: kubernetes.helm.v3.Release;
  public readonly namespace: pulumi.Output<string>;
  public readonly region: pulumi.Output<string>;
  public readonly hostname: pulumi.Output<string>;
  public readonly adminUsername: pulumi.Output<string>;
  public readonly adminPassword: pulumi.Output<string>;
  public readonly users: pulumi.Output<{
    [key: string]: { accessKey: pulumi.Output<string>; secretKey: pulumi.Output<string> };
  }>;

  /**
   * Create a new MinIO tenant resource.
   * @param name The name of the MinIO tenant resource.
   * @param args A bag of arguments to control the MinIO tenant creation.
   * @param opts A bag of options that control this resource's behavior.
   */
  constructor(name: string, args: TenantArgs, opts?: pulumi.ComponentResourceOptions) {
    super("minio:tenant", name, args, opts);

    const emptyOpts = this.opts();
    const k8sOpts = this.opts(k8s.Provider);

    if (args.region === undefined) {
      args.region = "minio";
    }

    this.adminUsername = pulumi.secret(
      new random.RandomPassword(`${name}-admin-username`, { length: 24, special: false }, emptyOpts).result,
    );
    this.adminPassword = pulumi.secret(
      new random.RandomPassword(`${name}-admin-password`, { length: 40, special: false }, emptyOpts).result,
    );

    this.namespace = args.namespace
      ? pulumi.output(args.namespace)
      : new k8s.core.v1.Namespace(name, { metadata: { name } }, k8sOpts).metadata.name;

    const adminCredentialsSecret = `${name}-admin-credentials`;

    this.region = pulumi.output(args.region);
    this.hostname = pulumi
      .all([args.apiIngress, this.namespace])
      .apply(([apiIngress, ns]) => apiIngress?.host ?? `${name}.${ns}.svc`);

    this.users = pulumi.all([args.users]).apply(([users]) =>
      pulumi.all(users ?? []).apply((users) => {
        const usersMap: { [key: string]: { accessKey: pulumi.Output<string>; secretKey: pulumi.Output<string> } } = {};

        for (const user of users ?? []) {
          const accessKey = pulumi.secret(
            new random.RandomPassword(`${name}-${user.name}-username`, { length: 24, special: false }, emptyOpts)
              .result,
          );

          const secretKey = pulumi.secret(
            new random.RandomPassword(`${name}-${user.name}-password`, { length: 48, special: false }, emptyOpts)
              .result,
          );

          // eslint-disable-next-line @typescript-eslint/no-unused-vars
          const secret = new kubernetes.v1.Secret(
            `${name}-user-${user.name}`,
            {
              metadata: { name: `${name}-user-${user.name}`, namespace: this.namespace },
              stringData: {
                CONSOLE_ACCESS_KEY: accessKey,
                CONSOLE_SECRET_KEY: secretKey,
              },
            },
            k8sOpts,
          );

          usersMap[user.name] = pulumi.output({ accessKey, secretKey });
        }

        return usersMap;
      }),
    );

    this.helmRelease = new kubernetes.helm.v3.Release(
      name,
      {
        namespace: this.namespace,
        repositoryOpts: { repo: "https://operator.min.io/" },
        chart: "tenant",
        values: {
          fullnameOverride: name,
          tenant: {
            name,
            configuration: { name: adminCredentialsSecret },
            configSecret: {
              name: adminCredentialsSecret,
              accessKey: this.adminUsername,
              secretKey: this.adminPassword,
            },
            env: [
              { name: "MINIO_STORAGE_CLASS_STANDARD", value: args.standardStorageClass ?? "EC:2" },
              { name: "MINIO_STORAGE_CLASS_RRS", value: args.reducedStorageClass ?? "EC:1" },
              { name: "MINIO_BROWSER", value: "on" },
              { name: "MINIO_BROWSER_LOGIN_ANIMATION", value: "off" },
              { name: "MINIO_BROWSER_REDIRECT", value: "false" },
              { name: "MINIO_PROMETHEUS_AUTH_TYPE", value: "public" },
              { name: "MINIO_PROMETHEUS_JOB_ID", value: "minio" },
              { name: "MINIO_UPDATE", value: "off" },
              { name: "MINIO_REGION", value: this.region },
              { name: "MINIO_SERVER_URL", value: this.hostname.apply((hostname) => `https://${hostname}/`) },
            ],
            logging: {
              anonymous: true,
              json: true,
              quiet: true,
            },
            pools: pulumi.all([args.pools]).apply(([pools]) => {
              return pools.map((pool) => {
                return {
                  name: pool.name,
                  servers: pool.servers,
                  volumesPerServer: pool.volumesPerServer ?? 1,
                  size: pool.size ?? "10Gi",
                  storageClassName: pool.storageClassName,
                  resources: pool.resources ?? args.resources,
                  affinity: {
                    podAntiAffinity: {
                      preferredDuringSchedulingIgnoredDuringExecution: [
                        {
                          weight: 100,
                          podAffinityTerm: {
                            labelSelector: {
                              matchExpressions: [{ key: "v1.min.io/tenant", operator: "In", values: [name] }],
                            },
                            topologyKey: "kubernetes.io/hostname",
                          },
                        },
                      ],
                    },
                  },
                };
              });
            }),
            features: {
              domains: {
                minio: pulumi
                  .all([args.domains])
                  .apply(([domains]) => (domains ?? []).map((domain) => `https://${domain}`)),
                console: pulumi
                  .all([args.consoleIngress, this.hostname])
                  .apply(([consoleIngress, hostname]) => `https://${consoleIngress?.host ?? hostname}`),
              },
            },
            buckets: pulumi.all([args.buckets]).apply(([buckets]) =>
              pulumi.all(buckets ?? []).apply((buckets) => {
                return buckets.map((bucket) => {
                  return {
                    name: bucket.name,
                    objectLock: bucket.objectLock ?? false,
                  };
                });
              }),
            ),
            users: pulumi.all([args.users]).apply(([users]) =>
              pulumi.all(users ?? []).apply((users) => {
                return users.map((user) => {
                  return {
                    name: `${name}-user-${user.name}`,
                  };
                });
              }),
            ),
            metrics: { enabled: false },
            prometheusOperator: false,
            certificate: {
              requestAutoCert: true,
            },
          },
          ingress: {
            api: this.ingressValues(name, "api", args.apiIngress),
            console: this.ingressValues(name, "console", args.consoleIngress),
          },
        },
      },
      { ...k8sOpts, protect: args.protect },
    );

    pulumi.all([args.bucketsIngress]).apply(([bucketsIngress]) => {
      if (bucketsIngress === undefined || bucketsIngress.hosts.length === 0) {
        return;
      }

      // eslint-disable-next-line @typescript-eslint/no-unused-vars
      const bucketsSecureIngress = new kubernetes.v1.SecureIngress(
        `${name}-buckets`,
        {
          metadata: { name: `${name}-buckets`, namespace: this.namespace, annotations: bucketsIngress.annotations },
          spec: {
            ingressClassName: bucketsIngress.ingressClassName,
            rules: bucketsIngress.hosts.map((host) => {
              return {
                host: host,
                http: {
                  paths: [{ path: "/", pathType: "Prefix", backend: { service: { name, port: { number: 443 } } } }],
                },
              };
            }),
          },
        },
        k8sOpts,
      );
    });

    this.registerOutputs();
  }

  private ingressValues(
    name: string,
    type: string,
    args?: pulumi.Input<IngressArgs>,
  ): pulumi.Output<internalIngressArgs> {
    return pulumi.all([args]).apply(([args]) => {
      if (args?.enabled === undefined || args.enabled === false) {
        return { enabled: false, host: "", tls: [] };
      }

      return {
        enabled: true,
        ingressClassName: args.ingressClassName,
        annotations: {
          "traefik.ingress.kubernetes.io/router.entrypoints": "websecure",
          "traefik.ingress.kubernetes.io/router.tls": "true",
          "cert-manager.io/cluster-issuer": Certificate.DefaultClusterIssuer,
          ...args.annotations,
        },
        host: args.host,
        tls: [{ hosts: [args.host], secretName: `${name}-${type}-tls` }],
      };
    });
  }
}
