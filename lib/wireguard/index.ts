import { Interface, Peer, WireguardArgs, Wireguard } from "./deployment.js";

export { Interface, Peer, WireguardArgs, Wireguard };
