import * as pulumi from "@pulumi/pulumi";

export const metadataNamespace = (namespace: pulumi.Input<string>) => {
  return (o: any, opts: pulumi.CustomResourceOptions): void => {
    if (o === undefined || o.apiVersion === undefined || o.kind === undefined) {
      return;
    }

    if (o.apiVersion === "v1" && o.kind === "List") {
      if (o.items === undefined) {
        return;
      }

      for (const item of o.items) {
        if (item.metadata !== undefined) {
          item.metadata.namespace = namespace;
        }
      }
    } else {
      if (o.metadata !== undefined) {
        o.metadata.namespace = namespace;
      }
    }
  };
};
