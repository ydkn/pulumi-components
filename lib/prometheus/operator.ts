import * as pulumi from "@pulumi/pulumi";
import * as k8s from "@pulumi/kubernetes";
import { ComponentResource } from "../common/index.js";
import * as kubernetes from "../kubernetes/index.js";

export interface SlackArgs {
  url: pulumi.Input<string>;
  channel: pulumi.Input<string>;
  sendResolved?: pulumi.Input<boolean>;
  iconURL?: pulumi.Input<string>;
  title?: pulumi.Input<string>;
  text?: pulumi.Input<string>;
}

export interface AlertmanagerArgs {
  serviceHostname?: pulumi.Input<string>;
  hostname?: pulumi.Input<string>;
  ingressClassName?: pulumi.Input<string>;
  slack?: pulumi.Input<SlackArgs>;
}

export interface OperatorArgs {
  namespace?: pulumi.Input<string>;
  serviceHostname?: pulumi.Input<string>;
  ingressClassName?: pulumi.Input<string>;
  hostname?: pulumi.Input<string>;
  alertmanager?: pulumi.Input<AlertmanagerArgs>;
  storage?: pulumi.Input<string>;
  retention?: pulumi.Input<string>;
}

export class Operator extends ComponentResource {
  private readonly helmRelease: kubernetes.helm.v3.Release;
  public readonly namespace: pulumi.Output<string>;
  public readonly url: pulumi.Output<string>;

  /**
   * Create a new Prometheus operator resource.
   * @param name The name of the Prometheus operator resource.
   * @param args A bag of arguments to control the Prometheus operator creation.
   * @param opts A bag of options that control this resource's behavior.
   */
  constructor(name: string, args: OperatorArgs, opts?: pulumi.ComponentResourceOptions) {
    super("prometheus:operator", name, args, opts);

    opts = this.opts(k8s.Provider);

    if (args.storage === undefined) {
      args.storage = "10Gi";
    }

    if (args.retention === undefined) {
      args.retention = "168h"; // 7 days
    }

    this.namespace = args.namespace
      ? pulumi.output(args.namespace)
      : new k8s.core.v1.Namespace(name, { metadata: { name } }, opts).metadata.name;

    this.url = pulumi.concat(
      "http://",
      pulumi.output(args.serviceHostname ?? pulumi.concat(name, "-prometheus", ".", this.namespace, ".", "svc")),
      ":9090",
    );

    this.helmRelease = new kubernetes.helm.v3.Release(
      name,
      {
        namespace: this.namespace,
        repositoryOpts: { repo: "https://prometheus-community.github.io/helm-charts" },
        chart: "kube-prometheus-stack",
        values: pulumi.all([args.alertmanager]).apply(([alertmanager]) => {
          return {
            fullnameOverride: name,
            prometheusOperator: {
              resources: {
                requests: { memory: "192Mi" },
                limits: { memory: "512Mi", cpu: "200m" },
              },
              prometheusConfigReloader: {
                resources: {
                  requests: { memory: "32Mi" },
                  limits: { memory: "48Mi", cpu: "200m" },
                },
              },
            },
            prometheus: {
              prometheusSpec: {
                retention: args.retention,
                logFormat: "json",
                externalUrl: args.hostname,
                enableRemoteWriteReceiver: true,
                remoteWriteDashboards: true,
                storageSpec: {
                  volumeClaimTemplate: {
                    spec: {
                      accessModes: ["ReadWriteOnce"],
                      resources: { requests: { storage: args.storage } },
                    },
                  },
                },
                additionalAlertManagerConfigs:
                  alertmanager?.hostname !== undefined
                    ? [{ scheme: "https", static_configs: [{ targets: [alertmanager.hostname] }] }]
                    : [],
                serviceMonitorSelector: { matchLabels: { prometheusNoScrape: "none" } },
                podMonitorSelector: { matchLabels: { prometheusNoScrape: "none" } },
                probeSelector: { matchLabels: { prometheusNoScrape: "none" } },
                ruleSelector: {},
                podAntiAffinity: "soft",
                resources: {
                  requests: { memory: "1024Mi" },
                  limits: { memory: "1280Mi" },
                },
              },
              service: {
                annotations:
                  args.serviceHostname !== undefined
                    ? { "external-dns.alpha.kubernetes.io/hostname": args.serviceHostname }
                    : {},
              },
              ingress: kubernetes.helm.v3.ingressValues(
                pulumi.interpolate`${name}-prometheus`,
                [args.hostname],
                args.ingressClassName,
              ),
            },
            alertmanager: {
              alertmanagerSpec: {
                externalUrl: pulumi.interpolate`https://${alertmanager?.hostname}`,
                retention: "120h",
                podAntiAffinity: "soft",
                resources: {
                  requests: { memory: "64Mi" },
                  limits: { memory: "128Mi" },
                },
              },
              config: {
                route: { receiver: alertmanager?.slack !== undefined ? "slack" : "null" },
                receivers: [{ name: "null" }, { name: "slack", slack_configs: this.slackConfigs(alertmanager?.slack) }],
              },
              service: {
                annotations:
                  alertmanager?.serviceHostname !== undefined
                    ? { "external-dns.alpha.kubernetes.io/hostname": alertmanager.serviceHostname }
                    : {},
              },
              ingress: kubernetes.helm.v3.ingressValues(
                pulumi.interpolate`${name}-alertmanager`,
                [alertmanager?.hostname],
                alertmanager?.ingressClassName ?? args.ingressClassName,
              ),
            },
            grafana: {
              enabled: false,
              forceDeployDatasources: true,
              forceDeployDashboards: false,
              deleteDatasources: [{ name: "Prometheus", orgId: 1 }],
              sidecar: {
                datasources: { url: pulumi.interpolate`http://${name}-prometheus.${this.namespace}.svc:9090` },
                dashboards: { multicluster: { global: { enabled: true }, etcd: { enabled: true } } },
              },
            },
            defaultRules: {
              create: true,
              rules: {
                etcd: false,
                kubeApiserverAvailability: false,
                kubeApiserverBurnrate: false,
                kubeApiserverHistogram: false,
                kubeApiserverSlos: false,
                kubeControllerManager: false,
                windows: false,
              },
            },
            kubeStateMetrics: { enabled: false },
            nodeExporter: { enabled: false },
            kubeApiServer: { enabled: false, serviceMonitor: { metricRelabelings: [] } },
            kubeControllerManager: { enabled: false, serviceMonitor: { metricRelabelings: [] } },
            kubeScheduler: { enabled: false, serviceMonitor: { metricRelabelings: [] } },
            kubeEtcd: { enabled: false, serviceMonitor: { metricRelabelings: [] } },
            kubeProxy: { enabled: false, serviceMonitor: { metricRelabelings: [] } },
            kubelet: { enabled: false, serviceMonitor: { metricRelabelings: [] } },
            coreDns: { enabled: false, serviceMonitor: { metricRelabelings: [] } },
          };
        }),
      },
      opts,
    );

    this.registerOutputs();
  }

  // eslint-disable-next-line @typescript-eslint/no-explicit-any
  private slackConfigs(slack?: pulumi.Input<SlackArgs>): pulumi.Output<any>[] {
    if (slack === undefined) {
      return [];
    }

    return [
      pulumi.all([slack]).apply(([slack]) => {
        return {
          api_url: slack.url,
          send_resolved: slack.sendResolved ?? true,
          icon_url: slack.iconURL ?? "https://avatars3.githubusercontent.com/u/3380462",
          channel: slack.channel,
          color: [
            `{{ if eq .Status "firing" -}}`,
            `{{ if eq .CommonLabels.severity "warning" -}}`,
            "warning",
            `{{- else if eq .CommonLabels.severity "critical" -}}`,
            "danger",
            "{{- else -}}",
            "#439FE0",
            "{{- end -}}",
            "{{ else -}}",
            "good",
            "{{- end }}",
          ].join(""),
          title:
            slack.title ??
            [
              "[",
              "{{ .Status | toUpper -}}",
              `{{ if eq .Status "firing" }}:{{ .Alerts.Firing | len }}{{- end -}}`,
              "] ",
              "{{ .CommonLabels.alertname }}",
            ].join(""),
          text:
            slack.text ??
            [
              `{{- if eq .CommonLabels.severity "critical" -}}`,
              "*Severity:* `Critical`",
              `{{- else if eq .CommonLabels.severity "warning" -}}`,
              "*Severity:* `Warning`",
              `{{- else if eq .CommonLabels.severity "info" -}}`,
              "*Severity:* `Info`",
              "{{- end }}",
              "{{- if (index .Alerts 0).Annotations.summary }}",
              `{{- "\\n" -}}`,
              "*Summary:* ",
              "{{ (index .Alerts 0).Annotations.summary }}",
              "{{- end }}",
              "{{ range .Alerts }}",
              "{{- if .Annotations.description }}",
              `{{- "\\n" -}}`,
              "{{ .Annotations.description }}",
              `{{- "\\n" -}}`,
              "{{- end }}",
              "{{- if .Annotations.message }}",
              `{{- "\\n" -}}`,
              "{{ .Annotations.message }}",
              `{{- "\\n" -}}`,
              "{{- end }}",
              "{{- end }}",
            ].join(""),
          actions: [
            { type: "button", text: "Query :mag:", url: "{{ (index .Alerts 0).GeneratorURL }}" },
            {
              type: "button",
              text: "Dashboard :chart_with_upwards_trend:",
              url: "{{ (index .Alerts 0).Annotations.dashboard_url }}",
            },
            {
              type: "button",
              text: "Silence :no_bell:",
              url: [
                "{{ .ExternalURL }}/#/silences/new?filter=%7B",
                "{{- range .CommonLabels.SortedPairs -}}",
                `{{- if ne .Name "alertname" -}}`,
                `{{- .Name }}%3D"{{- .Value -}}"%2C%20`,
                "{{- end -}}",
                "{{- end -}}",
                `alertname%3D"{{- .CommonLabels.alertname -}}"%7D`,
              ].join(""),
            },
          ],
        };
      }),
    ];
  }
}
