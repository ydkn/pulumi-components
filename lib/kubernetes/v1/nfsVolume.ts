import * as pulumi from "@pulumi/pulumi";
import * as random from "@pulumi/random";
import * as k8s from "@pulumi/kubernetes";
import { ComponentResource } from "../../common/index.js";

export interface NfsVolumesMapEntry {
  server: pulumi.Input<string>;
  path: pulumi.Input<string>;
  mountOptions?: pulumi.Input<pulumi.Input<string>[]>;
}

export interface NfsVolumesMap {
  [key: string]: pulumi.Input<NfsVolumesMapEntry>;
}

export interface NfsVolumeConfig {
  nfsServer: pulumi.Input<string>;
  nfsPath: pulumi.Input<string>;
  mountOptions?: pulumi.Input<pulumi.Input<string>[]>;
}

export interface NfsVolumeArgs {
  metadata: pulumi.Input<k8s.types.input.meta.v1.ObjectMeta>;
  server: pulumi.Input<string>;
  path: pulumi.Input<string>;
  mountOptions?: pulumi.Input<pulumi.Input<string>[]>;
  storage?: pulumi.Input<string>;
  persistentVolumeReclaimPolicy?: pulumi.Input<string>;
}

export class NfsVolume extends ComponentResource {
  public readonly persistentVolume: k8s.core.v1.PersistentVolume;
  public readonly persistentVolumeClaim: k8s.core.v1.PersistentVolumeClaim;

  /**
   * Create a new NFS volume resource.
   * @param name The name of the NFS volume resource.
   * @param args A bag of arguments to control the NFS volume creation.
   * @param opts A bag of options that control this resource's behavior.
   */
  constructor(name: string, args: NfsVolumeArgs, opts?: pulumi.ComponentResourceOptions) {
    super("nfs-volume", name, args, opts);

    const storage = args.storage || "1Gi";
    const persistentVolumeReclaimPolicy = args.persistentVolumeReclaimPolicy || "Delete";

    const emptyOpts = this.opts();
    const k8sOpts = this.opts(k8s.Provider);

    const persistentVolumeUUID = new random.RandomUuid(name, {}, emptyOpts);

    this.persistentVolume = new k8s.core.v1.PersistentVolume(
      name,
      {
        metadata: {
          ...args.metadata,
          name: pulumi.all([persistentVolumeUUID.result]).apply(([uuid]) => `nfs-${uuid}`),
          namespace: undefined,
        },
        spec: {
          accessModes: ["ReadWriteMany"],
          capacity: { storage },
          mountOptions: args.mountOptions,
          nfs: { server: args.server, path: args.path },
          persistentVolumeReclaimPolicy,
          storageClassName: "",
        },
      },
      k8sOpts,
    );

    this.persistentVolumeClaim = new k8s.core.v1.PersistentVolumeClaim(
      name,
      {
        metadata: args.metadata,
        spec: {
          accessModes: ["ReadWriteMany"],
          storageClassName: "",
          volumeName: this.persistentVolume.metadata.name,
          resources: { requests: { storage } },
        },
      },
      k8sOpts,
    );

    this.registerOutputs();
  }

  public static fromConfig(
    name: string,
    args: {
      metadata: pulumi.Input<k8s.types.input.meta.v1.ObjectMeta>;
      config: pulumi.Input<NfsVolumeConfig>;
      storage?: pulumi.Input<string>;
      persistentVolumeReclaimPolicy?: pulumi.Input<string>;
    },
    opts?: pulumi.ComponentResourceOptions,
  ): NfsVolume {
    return new NfsVolume(
      name,
      {
        metadata: args.metadata,
        server: pulumi.all([args.config]).apply(([config]) => config.nfsServer),
        path: pulumi.all([args.config]).apply(([config]) => config.nfsPath),
        mountOptions: pulumi.all([args.config]).apply(([config]) => config.mountOptions ?? []),
        storage: args.storage,
        persistentVolumeReclaimPolicy: args.persistentVolumeReclaimPolicy,
      },
      opts,
    );
  }
}
