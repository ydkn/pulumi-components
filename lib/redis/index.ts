import { OperatorArgs, Operator } from "./operator.js";
import { StandaloneArgs, Standalone } from "./standalone.js";

export { OperatorArgs, Operator, StandaloneArgs, Standalone };
