import * as pulumi from "@pulumi/pulumi";
import * as random from "@pulumi/random";
import * as k8s from "@pulumi/kubernetes";
import { ComponentResource } from "../common/index.js";
import * as kubernetes from "../kubernetes/index.js";

export const ActionProvision = "provision";
export const ActionJoin = "join";

export interface SambaDcArgs {
  namespace?: pulumi.Input<string>;
  action?: pulumi.Input<string>;
  serverName: pulumi.Input<string>;
  netbiosName: pulumi.Input<string>;
  workgroup: pulumi.Input<string>;
  domain: pulumi.Input<string>;
  storageClassName?: pulumi.Input<string>;
  storage?: pulumi.Input<string>;
  dnsServiceType?: pulumi.Input<string>;
  dnsLoadBalancerClass?: pulumi.Input<string>;
  nsHostname: pulumi.Input<string>;
  ldapHostname: pulumi.Input<string>;
  serviceLabels?: pulumi.Input<{ [key: string]: pulumi.Input<string> }>;
  nodeSelector?: pulumi.Input<{ [key: string]: pulumi.Input<string> }>;
  calicoIpReservations?: pulumi.Input<pulumi.Input<string>[]>;
  domainNameservers?: pulumi.Input<pulumi.Input<string>[]>;
  domainJoinUsername?: pulumi.Input<string>;
  domainJoinPassword?: pulumi.Input<string>;
  protect?: boolean;
}

export class SambaDc extends ComponentResource {
  private static ports = [
    { port: 53, protocol: "UDP", name: "dns-udp" },
    { port: 53, protocol: "TCP", name: "dns-tcp" },
    { port: 88, protocol: "UDP", name: "krb-udp" },
    { port: 88, protocol: "TCP", name: "krb-tcp" },
    { port: 135, protocol: "TCP", name: "dce" },
    { port: 137, protocol: "UDP", name: "netbios-ns" },
    { port: 138, protocol: "UDP", name: "netbios-dgm" },
    { port: 139, protocol: "TCP", name: "netbios-ssn" },
    { port: 389, protocol: "UDP", name: "ldap-udp" },
    { port: 389, protocol: "TCP", name: "ldap-tcp" },
    { port: 445, protocol: "TCP", name: "smb" },
    { port: 464, protocol: "UDP", name: "krb-kpasswd-udp" },
    { port: 464, protocol: "TCP", name: "krb-kpasswd-tcp" },
    { port: 636, protocol: "TCP", name: "ldaps" },
    { port: 3268, protocol: "TCP", name: "catalog" },
    { port: 3269, protocol: "TCP", name: "catalog-ssl" },
  ];

  private readonly hostname: pulumi.Output<string>;
  private readonly adminPasswordSecret: k8s.core.v1.Secret;
  private readonly config: k8s.core.v1.ConfigMap;
  private readonly calicoIpReservations?: k8s.apiextensions.CustomResource;
  private readonly dataVolumeClaim: k8s.core.v1.PersistentVolumeClaim;
  private readonly deployment: k8s.apps.v1.Deployment;
  private readonly podDisruptionBudget: k8s.policy.v1.PodDisruptionBudget;
  private readonly service: k8s.core.v1.Service;
  public readonly namespace: pulumi.Output<string>;
  public readonly baseDN: pulumi.Output<string>;
  public readonly nsHostname: pulumi.Output<string>;
  public readonly ldapHostname: pulumi.Output<string>;
  public readonly administratorDN: pulumi.Output<string>;
  public readonly workgroup: pulumi.Output<string>;
  public readonly domain: pulumi.Output<string>;
  public readonly domainNameservers: pulumi.Output<pulumi.Output<string>[]>;
  public readonly administratorUsername: pulumi.Output<string>;
  public readonly administratorPassword: pulumi.Output<string>;

  /**
   * Create a new Samba DC resource.
   * @param name The name of the Samba DC resource.
   * @param args A bag of arguments to control the Samba DC creation.
   * @param opts A bag of options that control this resource's behavior.
   */
  constructor(name: string, args: SambaDcArgs, opts?: pulumi.ComponentResourceOptions) {
    super("samba:dc", name, args, opts);

    if (args.action === undefined) {
      args.action = ActionProvision;
    }

    if (args.storage === undefined) {
      args.storage = "10Gi";
    }

    if (args.dnsServiceType === undefined) {
      args.dnsServiceType = "ClusterIP";
    }

    const k8sOpts = this.opts(k8s.Provider);

    opts = this.opts();

    this.hostname = pulumi.interpolate`${args.serverName}.${args.domain}`;
    this.workgroup = pulumi.output(args.workgroup);
    this.domain = pulumi.output(args.domain);

    this.namespace = args.namespace
      ? pulumi.output(args.namespace)
      : new k8s.core.v1.Namespace(name, { metadata: { name } }, k8sOpts).metadata.name;

    const metadata = {
      name,
      namespace: this.namespace,
      labels: {
        "app.kubernetes.io/name": "samba",
        "app.kubernetes.io/component": "dc",
        "app.kubernetes.io/instance": args.serverName,
      },
    };

    this.baseDN = pulumi.all([args.domain]).apply(([domain]) => `DC=${domain.replace(/\./g, ",DC=")}`);
    this.nsHostname = pulumi.output(args.nsHostname);
    this.ldapHostname = pulumi.output(args.ldapHostname);

    if (args.action === ActionJoin) {
      if (args.domainJoinUsername === undefined || args.domainJoinPassword === undefined) {
        throw new Error("domainJoinUsername and domainJoinPassword are required for join action");
      }

      this.administratorUsername = pulumi.secret(args.domainJoinUsername);
      this.administratorPassword = pulumi.secret(args.domainJoinPassword);
    } else {
      this.administratorUsername = pulumi.secret(pulumi.output("Administrator"));
      this.administratorPassword = pulumi.secret(
        new random.RandomPassword(`${name}-admin-password`, { length: 24, special: true }, opts).result,
      );
    }

    this.administratorDN = pulumi.secret(pulumi.interpolate`CN=${this.administratorUsername},CN=Users,${this.baseDN}`);

    this.adminPasswordSecret = new k8s.core.v1.Secret(
      `${name}-admin-password`,
      {
        metadata: { ...metadata, name: pulumi.interpolate`${metadata.name}-admin-password` },
        stringData: {
          "samba-admin-password": this.administratorPassword,
        },
      },
      k8sOpts,
    );

    this.config = new k8s.core.v1.ConfigMap(
      name,
      {
        metadata,
        data: {
          "00globals.conf": ["[global]", "ldap server require strong auth = no", "idmap_ldb:use rfc2307 = yes"].join(
            "\n",
          ),
        },
      },
      k8sOpts,
    );

    if (args.calicoIpReservations !== undefined) {
      this.calicoIpReservations = new k8s.apiextensions.CustomResource(
        name,
        {
          apiVersion: "crd.projectcalico.org/v1",
          kind: "IPReservation",
          metadata: { ...metadata, namespace: undefined },
          spec: {
            reservedCIDRs: args.calicoIpReservations,
          },
        },
        k8sOpts,
      );
    }

    const dataVolumeClaimName = pulumi.interpolate`${metadata.name}-data`;

    this.dataVolumeClaim = new k8s.core.v1.PersistentVolumeClaim(
      `${name}-data`,
      {
        metadata: { ...metadata, name: dataVolumeClaimName },
        spec: {
          storageClassName: args.storageClassName,
          accessModes: ["ReadWriteOnce"],
          resources: { requests: { storage: args.storage } },
        },
      },
      { ...k8sOpts, protect: args.protect },
    );

    this.deployment = new k8s.apps.v1.Deployment(
      name,
      {
        metadata,
        spec: {
          selector: { matchLabels: metadata.labels },
          strategy: { type: "Recreate" },
          replicas: 1,
          template: {
            metadata: {
              labels: metadata.labels,
              annotations: {
                "backup.velero.io/backup-volumes": "data",
                "cni.projectcalico.org/ipAddrs": pulumi.all([args.calicoIpReservations]).apply(([ipReservations]) => {
                  if (ipReservations === undefined) {
                    return pulumi.output("");
                  }

                  return pulumi
                    .all(ipReservations)
                    .apply(
                      (ipReservations) => `[${ipReservations.map((ipReservation) => `"${ipReservation}"`).join(",")}]`,
                    );
                }),
              },
            },
            spec: {
              nodeSelector: args.nodeSelector,
              tolerations: [
                { key: "node-role.kubernetes.io/master", operator: "Exists", effect: "NoSchedule" },
                { key: "node-role.kubernetes.io/control-plane", operator: "Exists", effect: "NoSchedule" },
              ],
              hostname: pulumi.all([args.serverName]).apply(([serverName]) => serverName.toLowerCase()),
              dnsPolicy: args.domainNameservers ? "None" : undefined,
              dnsConfig: {
                nameservers: args.domainNameservers,
                searches: args.domain ? [args.domain] : undefined,
              },
              containers: [
                {
                  name: "samba-dc",
                  image: "instantlinux/samba-dc:latest",
                  imagePullPolicy: "Always",
                  securityContext: {
                    capabilities: { add: ["SYS_ADMIN"] },
                  },
                  env: [
                    { name: "ALLOW_DNS_UPDATES", value: "secure" },
                    { name: "BIND_INTERFACES_ONLY", value: "no" },
                    { name: "DNS_FORWARDER", value: "1.1.1.1" },
                    { name: "DOMAIN_ACTION", value: args.action },
                    { name: "DOMAIN_LOGONS", value: "yes" },
                    { name: "DOMAIN_MASTER", value: "yes" },
                    { name: "INTERFACES", value: "eth0" },
                    { name: "NETBIOS_NAME", value: args.netbiosName },
                    { name: "REALM", value: args.domain },
                    { name: "SERVER_STRING", value: args.serverName },
                    { name: "WINBIND_USE_DEFAULT_DOMAIN", value: "yes" },
                    { name: "WORKGROUP", value: args.workgroup },
                    { name: "TZ", value: "UTC" },
                  ],
                  ports: SambaDc.ports.map((port) => ({
                    name: port.name,
                    protocol: port.protocol,
                    containerPort: port.port,
                  })),
                  readinessProbe: {
                    tcpSocket: { port: "smb" },
                    periodSeconds: 30,
                    failureThreshold: 3,
                    successThreshold: 1,
                    timeoutSeconds: 10,
                  },
                  livenessProbe: {
                    tcpSocket: { port: "smb" },
                    periodSeconds: 30,
                    failureThreshold: 10,
                    successThreshold: 1,
                    timeoutSeconds: 10,
                  },
                  resources: {
                    requests: { memory: "256Mi" },
                    limits: { memory: "384Mi" },
                  },
                  volumeMounts: [
                    {
                      name: "samba-admin-password",
                      mountPath: "/run/secrets/samba-admin-password",
                      subPath: "samba-admin-password",
                    },
                    { name: "config", mountPath: "/etc/samba/conf.d/00globals.conf", subPath: "00globals.conf" },
                    { name: "data", mountPath: "/var/lib/samba" },
                  ],
                },
              ],
              volumes: [
                { name: "samba-admin-password", secret: { secretName: this.adminPasswordSecret.metadata.name } },
                { name: "config", configMap: { name: this.config.metadata.name } },
                { name: "data", persistentVolumeClaim: { claimName: dataVolumeClaimName } },
              ],
            },
          },
        },
      },
      k8sOpts,
    );

    this.podDisruptionBudget = new k8s.policy.v1.PodDisruptionBudget(
      name,
      {
        metadata,
        spec: {
          minAvailable: 1,
          selector: { matchLabels: metadata.labels },
        },
      },
      k8sOpts,
    );

    this.service = new kubernetes.v1.Service(
      name,
      {
        metadata: {
          ...metadata,
          labels: pulumi.all([args.serviceLabels]).apply(([labels]) => {
            return { ...metadata.labels, ...labels };
          }),
        },
        spec: {
          type: args.dnsServiceType,
          loadBalancerClass: args.dnsLoadBalancerClass,
          selector: metadata.labels,
          ports: SambaDc.ports.map((port) => ({
            name: port.name,
            protocol: port.protocol,
            port: port.port,
            targetPort: port.name,
          })),
        },
        hostnames: [args.nsHostname, args.ldapHostname],
      },
      { ...k8sOpts, dependsOn: [this.deployment] },
    );

    this.domainNameservers = pulumi
      .all([args.domainNameservers, this.service.status.loadBalancer])
      .apply(([domainNameservers, loadBalancer]) => {
        const addresses: string[] = [];

        if (loadBalancer?.ingress !== undefined) {
          for (const ingress of loadBalancer.ingress) {
            if (ingress.ip !== undefined && /^[0-9]+\.[0-9]+\.[0-9]+\.[0-9]+$/g.test(ingress.ip)) {
              addresses.push(ingress.ip);
            }
          }
        }

        if (domainNameservers !== undefined) {
          addresses.push(...domainNameservers);
        }

        return addresses.map((address) => pulumi.output(address));
      });

    this.registerOutputs();
  }
}
