import * as pulumi from "@pulumi/pulumi";
import * as random from "@pulumi/random";
import * as tls from "@pulumi/tls";
import * as hcloud from "@pulumi/hcloud";
import * as k8s from "@pulumi/kubernetes";
import * as command from "@pulumi/command";
import * as fs from "fs";
import * as path from "path";
import * as url from "url";
import * as crypto from "crypto";
import { ComponentResource } from "../common/index.js";
import * as node from "../node/index.js";
import * as fcos from "../fcos/index.js";
import * as flatcar from "../flatcar/index.js";
import * as k0s from "../k0s/index.js";
import { Kubernetes } from "./kubernetes.js";

export interface Node extends node.Node, node.IgnitionArgs {
  serverType: pulumi.Input<string>;
  count?: pulumi.Input<number>;
  location?: pulumi.Input<string>;
}

export interface ClusterArgs extends k0s.ClusterArgs, node.IgnitionArgs {
  name?: pulumi.Input<string>;
  token: pulumi.Input<string>;
  location: pulumi.Input<string>;
  zone: pulumi.Input<string>;
  networkRange: pulumi.Input<string>;
  nodeRange: pulumi.Input<string>;
  nodes: pulumi.Input<pulumi.Input<Node>[]>;
  externalSshAccess?: pulumi.Input<boolean>;
  trustedNetworkRanges?: pulumi.Input<pulumi.Input<string>[]>;
  externalAccess?: pulumi.Input<boolean>;
  sshAuthorizedKeys?: pulumi.Input<pulumi.Input<string>[]>;
}

export class Cluster extends ComponentResource {
  private static readonly rescueSshUsername = "root";
  private static readonly defaultOperatingSystem = "fcos";
  private static readonly sshUsername = "pulumi";
  private static readonly interfacesMap = {
    fcos: {
      DEFAULT: { primary: "enp1s0", private: [7, 8].map((i) => `enp${i}s0`) },
      "^cx\\d+1$": { primary: "ens3", private: [10, 11].map((i) => `ens${i}`) },
      "^cx\\d+2$": { primary: "enp1s0", private: [7, 8].map((i) => `enp${i}s0`) },
      "^cax\\d+1$": { primary: "enp1s0", private: [7, 8].map((i) => `enp${i}s0`) },
    },
    flatcar: {
      DEFAULT: { primary: "eth0", private: [1, 2].map((i) => `eth${i}`) },
    },
  };
  private static readonly dnsServers = ["2a01:4ff:ff00::add:1", "2a01:4ff:ff00::add:2"];
  private static readonly gatewayIPv6 = "fe80::1";
  private static readonly ignitionConfigPathRemote = "/tmp/config.ign";
  private static readonly installScriptPathRemote = "/tmp/install.sh";

  private readonly sshKey: hcloud.SshKey;
  private readonly network: hcloud.Network;
  private readonly nodeSubnet: hcloud.NetworkSubnet;
  private readonly placementGroup: hcloud.PlacementGroup;
  private readonly sshFirewall: hcloud.Firewall;
  private readonly nodesFirewall: hcloud.Firewall;
  private readonly kubernetes: Kubernetes;
  public readonly name: pulumi.Output<string>;
  public readonly location: pulumi.Output<string>;
  public readonly zone: pulumi.Output<string>;
  public readonly sshPrivateKey: tls.PrivateKey;
  public readonly sshPublicKey: pulumi.Output<string>;
  public readonly networkId: pulumi.Output<number>;
  public readonly networkRange: pulumi.Output<string>;
  public readonly nodeRange: pulumi.Output<string>;
  public readonly nodes: pulumi.Output<pulumi.Output<node.Node>[]>;
  public readonly kubeconfig: pulumi.Output<string>;
  public readonly provider: k8s.Provider;
  public readonly kubeletDirPath: pulumi.Output<string>;

  /**
   * Create a new hcloud cluster resource.
   * @param name The name of hcloud cluster resource.
   * @param args A bag of arguments to control the hcloud cluster creation.
   * @param opts A bag of options that control this resource's behavior.
   */
  constructor(name: string, args: ClusterArgs, opts?: pulumi.ComponentResourceOptions) {
    super("hcloud:cluster", name, args, opts);

    const emptyOpts = this.opts();
    const hcloudOpts = this.opts(hcloud.Provider);

    if (args.name === undefined) {
      this.name = new random.RandomId(name, { byteLength: 3 }, { ...emptyOpts, protect: args.protect }).hex;
    } else {
      this.name = pulumi.output(args.name);
    }

    this.location = pulumi.output(args.location);
    this.zone = pulumi.output(args.zone);

    this.sshPrivateKey = new tls.PrivateKey(name, { algorithm: "ED25519" }, { ...emptyOpts, protect: args.protect });
    this.sshPublicKey = this.sshPrivateKey.publicKeyOpenssh;
    this.sshKey = new hcloud.SshKey(name, { name: "pulumi", publicKey: this.sshPublicKey }, hcloudOpts);

    this.network = new hcloud.Network(
      name,
      { name: "private", ipRange: args.networkRange, labels: { cluster: this.name } },
      { ...hcloudOpts, protect: args.protect },
    );
    this.networkId = this.network.id.apply((o) => parseInt(o));
    this.networkRange = this.network.ipRange;

    this.nodeSubnet = new hcloud.NetworkSubnet(
      `${name}-nodes`,
      { type: "cloud", networkId: this.networkId, networkZone: this.zone, ipRange: args.nodeRange },
      { ...hcloudOpts, protect: args.protect },
    );
    this.nodeRange = this.nodeSubnet.ipRange;

    this.placementGroup = new hcloud.PlacementGroup(
      name,
      { name: "nodes", type: "spread", labels: { cluster: this.name } },
      hcloudOpts,
    );

    const serverImage = pulumi
      .all([hcloud.getImages({ withStatuses: ["available"] }, hcloudOpts)])
      .apply(([result]) => {
        return result.images.filter((image) => image.osFlavor === "ubuntu").sort((a, b) => (a > b ? -1 : 1))[0].name;
      });

    const servers = this.createServers(
      args.nodes,
      serverImage,
      this.sshKey,
      this.sshPrivateKey.privateKeyOpenssh,
      this.nodeSubnet,
      this.placementGroup,
      args.files,
      args.links,
      args.systemdUnits,
      args.sshAuthorizedKeys,
      args.protect,
    );

    this.nodes = servers.apply((servers) => servers.map((s) => s.node));

    const nodeIPs = pulumi.all([this.nodes]).apply(([nodes]) =>
      pulumi.all(nodes).apply((nodes) => {
        const nodeIPs: pulumi.Output<string>[] = [];

        for (const node of nodes) {
          nodeIPs.push(pulumi.interpolate`${node.ipv4Address}/32`, pulumi.interpolate`${node.ipv6Address}/128`);

          if (node.privateIpv4Address !== undefined) {
            nodeIPs.push(pulumi.interpolate`${node.privateIpv4Address}/32`);
          }
        }

        if (nodeIPs.length <= 0) {
          nodeIPs.push(pulumi.output("127.0.0.1/32"));
        }

        return nodeIPs;
      }),
    );

    this.sshFirewall = new hcloud.Firewall(
      `${name}-ssh`,
      {
        name: "ssh",
        labels: { cluster: this.name },
        applyTos: [{ labelSelector: pulumi.interpolate`cluster=${this.name}` }],
        rules: [
          {
            description: "SSH",
            direction: "in",
            protocol: "tcp",
            port: "22",
            sourceIps: pulumi
              .all([args.externalSshAccess])
              .apply(([externalSshAccess]) =>
                externalSshAccess ? ["0.0.0.0/0", "::/0"] : ["127.0.0.1/32", "::1/128"],
              ),
          },
        ],
      },
      hcloudOpts,
    );

    const nodeFirewallSourceIps = nodeIPs.apply((nodeIPs) => nodeIPs.concat(this.networkRange));

    this.nodesFirewall = new hcloud.Firewall(
      `${name}-nodes`,
      {
        name: "nodes",
        labels: { cluster: this.name },
        applyTos: [{ labelSelector: pulumi.interpolate`cluster=${this.name}` }],
        rules: pulumi
          .all([args.trustedNetworkRanges ?? [], nodeFirewallSourceIps])
          .apply(([trustedNetworkRanges, nodeFirewallSourceIps]) =>
            pulumi.all(trustedNetworkRanges.concat(nodeFirewallSourceIps)).apply((sourceIps) => [
              { description: "ICMP", direction: "in", protocol: "icmp", sourceIps },
              { description: "TCP", direction: "in", protocol: "tcp", port: "any", sourceIps },
              { description: "UDP", direction: "in", protocol: "udp", port: "any", sourceIps },
            ]),
          ),
      },
      hcloudOpts,
    );

    pulumi.all([args.externalAccess]).apply(([externalAccess]) => {
      if (externalAccess) {
        // eslint-disable-next-line @typescript-eslint/no-unused-vars
        const controllersFirewall = new hcloud.Firewall(
          `${name}-controllers`,
          {
            name: "controllers",
            labels: { cluster: this.name, controller: "true" },
            applyTos: [{ labelSelector: pulumi.interpolate`cluster=${this.name},role=controller` }],
            rules: [
              { description: "TCP", direction: "in", protocol: "tcp", port: "6443", sourceIps: ["0.0.0.0/0", "::/0"] },
            ],
          },
          hcloudOpts,
        );
      }
    });

    const k0sCluster = new k0s.Cluster(
      name,
      {
        name: this.name,
        nodes: this.nodes.apply((nodes) => pulumi.all(nodes).apply((nodes) => nodes.filter((node) => !node.disabled))),
        k0s: pulumi.all([args.k0s]).apply(([k0s]) => {
          if (k0s === undefined) {
            k0s = {};
          }

          if (k0s.config === undefined) {
            k0s.config = {};
          }

          if (k0s.config.spec === undefined) {
            k0s.config.spec = {};
          }

          if (k0s.config.spec.network === undefined) {
            k0s.config.spec.network = {};
          }

          if (k0s.config.spec.network.provider === undefined) {
            k0s.config.spec.network.provider = "calico";
          }

          if (k0s.config.spec.network.provider === "calico") {
            if (k0s.config.spec.network.calico === undefined) {
              k0s.config.spec.network.calico = {};
            }

            if (k0s.config.spec.network.calico.mode === undefined) {
              k0s.config.spec.network.calico.mode = "bird";
            }

            if (k0s.config.spec.network.calico.wireguard === undefined) {
              k0s.config.spec.network.calico.wireguard = true;
            }

            if (k0s.config.spec.network.calico.ipAutodetectionMethod === undefined) {
              const interfaces = new Set<string>();
              for (const os of Object.values(Cluster.interfacesMap)) {
                for (const iface of Object.values(os)) {
                  interfaces.add(iface.primary);
                  for (const privateIface of iface.private) {
                    interfaces.add(privateIface);
                  }
                }
              }

              k0s.config.spec.network.calico.ipAutodetectionMethod = `interface=^(${Array.from(interfaces).join("|")})$`;
            }
          }

          return k0s;
        }),
        cloudProvider: true,
        protect: args.protect,
      },
      { ...emptyOpts, dependsOn: servers.apply((servers) => pulumi.all(servers.map((s) => s.depends))) },
    );

    this.kubeletDirPath = k0sCluster.kubeletDirPath;
    this.kubeconfig = k0sCluster.kubeconfig;
    this.provider = new k8s.Provider(
      `${name}-k8s`,
      { kubeconfig: this.kubeconfig, suppressHelmHookWarnings: true },
      emptyOpts,
    );

    this.kubernetes = new Kubernetes(
      name,
      {
        token: args.token,
        network: this.network.id,
        podCIDR: k0sCluster.spec.apply((spec) =>
          spec.k0s
            ? [spec.k0s.config?.spec?.network?.podCIDR, spec.k0s.config?.spec?.network?.dualStack?.IPv6podCIDR]
                .filter((cidr) => cidr !== undefined)
                .join(",")
            : "",
        ),
        kubeletDirPath: this.kubeletDirPath,
      },
      { ...emptyOpts, providers: [this.provider] },
    );

    this.registerOutputs();
  }

  private createServers(
    nodes: pulumi.Input<pulumi.Input<Node>[]>,
    image: pulumi.Input<string>,
    sshKey: pulumi.Input<hcloud.SshKey>,
    sshPrivateKey: pulumi.Input<string>,
    subnet: hcloud.NetworkSubnet,
    placementGroup: hcloud.PlacementGroup,
    files?: pulumi.Input<pulumi.Input<node.IgnitionFileArgs>[]>,
    links?: pulumi.Input<pulumi.Input<node.IgnitionLinkArgs>[]>,
    systemdUnits?: pulumi.Input<pulumi.Input<node.IgnitionSystemdUnitArgs>[]>,
    sshAuthorizedKeys?: pulumi.Input<pulumi.Input<string>[]>,
    protect?: boolean,
  ): pulumi.Output<{ node: pulumi.Output<node.Node>; depends: pulumi.Output<pulumi.Resource> }[]> {
    const emptyOpts = this.opts();
    const hcloudOpts = this.opts(hcloud.Provider);

    return pulumi.all([nodes]).apply(([nodes]) => {
      const results: { node: pulumi.Output<node.Node>; depends: pulumi.Output<pulumi.Resource> }[] = [];

      for (const nodeGroup of nodes) {
        let nodesCount = nodeGroup.count;
        if (nodesCount === undefined) {
          nodesCount = 1;
        }

        nodeGroup.operatingSystem = nodeGroup.operatingSystem ?? Cluster.defaultOperatingSystem;

        const roles: string[] = nodeGroup.roles ?? [];

        let isController = false;
        for (const role of roles) {
          if (role === "controller") {
            isController = true;
            break;
          }
        }

        if (isController) {
          roles.push("etcd");
        }

        for (let i = 0; i < nodesCount; i++) {
          const nodeId = new random.RandomId(`${nodeGroup.id}-${i}`, { byteLength: 3 }, emptyOpts).hex;
          const hostname = pulumi.interpolate`${this.name}-${k0s.Cluster.rolesHostname(nodeGroup.roles)}-${nodeId}`;

          const server = pulumi.all([this.name, hostname, nodeGroup.roles, sshKey, nodeId, placementGroup.id]).apply(
            ([name, hostname, roles, sshKey, nodeId, placementGroupId]) =>
              new hcloud.Server(
                hostname,
                {
                  name: hostname,
                  location: nodeGroup.location ?? this.location,
                  serverType: nodeGroup.serverType,
                  image,
                  rescue: "linux64",
                  sshKeys: [sshKey.id],
                  keepDisk: true,
                  labels: { cluster: name, nodeId, role: k0s.Cluster.rolesHostname(roles) },
                  placementGroupId: parseInt(placementGroupId),
                },
                { ...hcloudOpts, ignoreChanges: ["image", "sshKeys"], protect },
              ),
          );

          let dataVolume: pulumi.Output<hcloud.Volume> | undefined = undefined;
          if (isController) {
            dataVolume = pulumi.all([this.name, hostname, nodeId, server.id]).apply(
              ([name, hostname, nodeId, serverId]) =>
                new hcloud.Volume(
                  `${hostname}-data`,
                  {
                    name: `${hostname}-data`,
                    size: 10,
                    format: "ext4",
                    labels: { cluster: name, nodeId, role: k0s.Cluster.rolesHostname(roles) },
                    serverId: parseInt(serverId),
                    deleteProtection: true,
                  },
                  { ...hcloudOpts, protect },
                ),
            );
          }

          const serverNetwork = pulumi.all([server.id, server.name]).apply(([serverId, serverName]) => {
            return new hcloud.ServerNetwork(
              serverName,
              { serverId: parseInt(serverId), subnetId: subnet.id },
              hcloudOpts,
            );
          });

          const node = pulumi
            .all([hostname, files, links, systemdUnits, sshAuthorizedKeys, server.ipv6Address, server.serverType])
            .apply(([hostname, files, links, systemdUnits, sshAuthorizedKeys, ipv6Address, serverType]) => {
              const ethernetInterfaces = Cluster.ethernetInterfaceName(nodeGroup.operatingSystem!, serverType);

              let host: fcos.Host | flatcar.Host;

              switch (nodeGroup.operatingSystem) {
                case "fcos":
                  host = new fcos.Host(
                    hostname,
                    {
                      hostname,
                      filesystems:
                        dataVolume !== undefined
                          ? [
                              {
                                device: pulumi.interpolate`/dev/disk/by-id/scsi-0HC_Volume_${dataVolume.id}`,
                                path: "/var/lib/k0s",
                                format: dataVolume.format.apply((format) => format ?? "ext4"),
                              },
                            ]
                          : [],
                      files: pulumi.all(files ?? []).apply((files) => {
                        files.push({
                          path: `/etc/NetworkManager/system-connections/${ethernetInterfaces.primary}.nmconnection`,
                          contents: [
                            "[connection]",
                            "type=ethernet",
                            `id=${ethernetInterfaces.primary}`,
                            `interface-name=${ethernetInterfaces.primary}`,
                            "[ipv4]",
                            "method=auto",
                            "ignore-auto-dns=true",
                            "[ipv6]",
                            "method=manual",
                            `address1=${ipv6Address}`,
                            `gateway=${Cluster.gatewayIPv6}`,
                            `dns=${Cluster.dnsServers.join(";")}`,
                          ].join("\n"),
                          mode: 0o600,
                        });

                        for (const iface of ethernetInterfaces.private) {
                          files.push({
                            path: `/etc/NetworkManager/system-connections/${iface}.nmconnection`,
                            contents: [
                              "[connection]",
                              "type=ethernet",
                              `id=${iface}`,
                              `interface-name=${iface}`,
                              "[ipv4]",
                              "method=auto",
                              "never-default=true",
                              "[ipv6]",
                              "method=auto",
                              "never-default=true",
                            ].join("\n"),
                            mode: 0o600,
                          });
                        }

                        return files;
                      }),
                      links,
                      systemdUnits,
                      users: [{ name: "core", sshAuthorizedKeys }],
                      sshUsers: [{ name: Cluster.sshUsername, groups: ["sudo"] }],
                    },
                    emptyOpts,
                  );

                  break;
                case "flatcar":
                  host = new flatcar.Host(
                    hostname,
                    {
                      hostname,
                      filesystems:
                        dataVolume !== undefined
                          ? [
                              {
                                device: pulumi.interpolate`/dev/disk/by-id/scsi-0HC_Volume_${dataVolume.id}`,
                                path: "/var/lib/k0s",
                                format: dataVolume.format.apply((format) => format ?? "ext4"),
                              },
                            ]
                          : [],
                      files: pulumi.all(files ?? []).apply((files) => {
                        files.push({
                          path: `/etc/systemd/network/10-${ethernetInterfaces.primary}.network`,
                          contents: [
                            "[Match]",
                            `Name=${ethernetInterfaces.primary}`,
                            "[Link]",
                            "RequiredForOnline=routable",
                            "[Network]",
                            "DHCP=yes",
                            `Address=${ipv6Address}/64`,
                            `Gateway=${Cluster.gatewayIPv6}`,
                            ...Cluster.dnsServers.map((dns) => `DNS=${dns}`),
                          ].join("\n"),
                          mode: 0o644,
                        });

                        for (const iface of ethernetInterfaces.private) {
                          files.push({
                            path: `/etc/systemd/network/10-${iface}.network`,
                            contents: ["[Match]", `Name=${iface}`, "[Network]", "DHCP=yes", "IPv6AcceptRA=true"].join(
                              "\n",
                            ),
                            mode: 0o644,
                          });
                        }

                        return files;
                      }),
                      links,
                      systemdUnits,
                      users: [{ name: "core", sshAuthorizedKeys }],
                      sshUsers: [{ name: Cluster.sshUsername, groups: ["sudo"] }],
                    },
                    emptyOpts,
                  );

                  break;
                default:
                  throw new Error(`Unsupported operating system ${nodeGroup.operatingSystem}`);
              }

              return {
                id: server.id,
                hostname,
                roles,
                ipv4Address: server.ipv4Address,
                privateIpv4Address: serverNetwork.ip,
                ipv6Address: server.ipv6Address,
                labels: nodeGroup.labels ?? {},
                taints: nodeGroup.taints ?? [],
                username: Cluster.sshUsername,
                privateKey: host.sshPrivateKeys.apply((sshPrivateKeys) => {
                  for (const username in sshPrivateKeys) {
                    if (username === Cluster.sshUsername) {
                      // eslint-disable-next-line security/detect-object-injection
                      return sshPrivateKeys[username].privateKeyOpenssh;
                    }
                  }

                  throw new Error(`No private key found for user ${Cluster.sshUsername}`);
                }),
                operatingSystem: nodeGroup.operatingSystem,
                ignitionConfig: host.ignitionConfig,
                disabled: nodeGroup.disabled ?? false,
              } as node.Node;
            });

          results.push({
            node,
            depends: pulumi.all([node.operatingSystem!]).apply(([os]) => {
              switch (os) {
                case "fcos":
                  return this.configureFcosNode(sshPrivateKey, server, node);
                case "flatcar":
                  return this.configureFlatcarNode(sshPrivateKey, server, node);
                default:
                  throw new Error(`Unsupported operating system ${os}`);
              }
            }),
          });
        }
      }

      return results;
    });
  }

  private configureFcosNode(
    sshPrivateKey: pulumi.Input<string>,
    server: pulumi.Output<hcloud.Server>,
    node: pulumi.Input<node.Node>,
  ): pulumi.Output<pulumi.Resource> {
    const emptyOpts = this.opts();

    return pulumi.all([server.name, server.ipv4Address, node]).apply(([nodeName, ipv4Address, node]) => {
      const connection: command.types.input.remote.ConnectionArgs = {
        host: ipv4Address,
        privateKey: sshPrivateKey,
        user: Cluster.rescueSshUsername,
        perDialTimeout: 10,
        dialErrorLimit: 30,
      };

      const fcosInstallScript = new command.remote.CopyToRemote(
        `${nodeName}-fcos-install-script`,
        {
          connection,
          source: new pulumi.asset.FileAsset(
            path.join(path.dirname(url.fileURLToPath(import.meta.url)), "assets", "fcos-install.sh"),
          ),
          remotePath: Cluster.installScriptPathRemote,
        },
        { ...emptyOpts, dependsOn: server, ignoreChanges: ["connection", "source", "remotePath"] },
      );

      const fcosConfigFile = new command.remote.CopyToRemote(
        `${nodeName}-fcos-config`,
        {
          connection,
          source: this.fileAssetFromString(node.ignitionConfig!),
          remotePath: Cluster.ignitionConfigPathRemote,
        },
        { ...emptyOpts, dependsOn: server, ignoreChanges: ["connection", "source", "remotePath"] },
      );

      const fcosInstall = new command.remote.Command(
        `${nodeName}-fcos-install`,
        {
          connection,
          create: [
            `chmod +x ${Cluster.installScriptPathRemote}`,
            `${Cluster.installScriptPathRemote} /dev/sda ${Cluster.ignitionConfigPathRemote}`,
          ].join(" && "),
        },
        { ...emptyOpts, dependsOn: [fcosInstallScript, fcosConfigFile], ignoreChanges: ["connection", "create"] },
      );

      return new command.remote.Command(
        `${nodeName}-fcos-up`,
        {
          connection: {
            host: ipv4Address,
            privateKey: node.privateKey,
            user: Cluster.sshUsername,
            perDialTimeout: 10,
            dialErrorLimit: 30,
          },
          create: "echo up",
        },
        { ...emptyOpts, dependsOn: fcosInstall, ignoreChanges: ["connection", "create"] },
      );
    });
  }

  private configureFlatcarNode(
    sshPrivateKey: pulumi.Input<string>,
    server: pulumi.Output<hcloud.Server>,
    node: pulumi.Input<node.Node>,
  ): pulumi.Output<pulumi.Resource> {
    const emptyOpts = this.opts();

    return pulumi.all([server.name, server.ipv4Address, node]).apply(([nodeName, ipv4Address, node]) => {
      const connection: command.types.input.remote.ConnectionArgs = {
        host: ipv4Address,
        privateKey: sshPrivateKey,
        user: Cluster.rescueSshUsername,
        perDialTimeout: 10,
        dialErrorLimit: 30,
      };

      const flatcarInstallScript = new command.remote.CopyToRemote(
        `${nodeName}-flatcar-install-script`,
        {
          connection,
          source: new pulumi.asset.FileAsset(
            path.join(path.dirname(url.fileURLToPath(import.meta.url)), "assets", "flatcar-install.sh"),
          ),
          remotePath: Cluster.installScriptPathRemote,
        },
        { ...emptyOpts, dependsOn: server, ignoreChanges: ["connection", "source", "remotePath"] },
      );

      const flatcarConfigFile = new command.remote.CopyToRemote(
        `${nodeName}-flatcar-config`,
        {
          connection,
          source: this.fileAssetFromString(node.ignitionConfig!),
          remotePath: Cluster.ignitionConfigPathRemote,
        },
        { ...emptyOpts, dependsOn: server, ignoreChanges: ["connection", "source", "remotePath"] },
      );

      const flatcarInstall = new command.remote.Command(
        `${nodeName}-flatcar-install`,
        {
          connection,
          create: [
            `chmod +x ${Cluster.installScriptPathRemote}`,
            `${Cluster.installScriptPathRemote} /dev/sda ${Cluster.ignitionConfigPathRemote}`,
          ].join(" && "),
        },
        { ...emptyOpts, dependsOn: [flatcarInstallScript, flatcarConfigFile], ignoreChanges: ["connection", "create"] },
      );

      return new command.remote.Command(
        `${nodeName}-flatcar-up`,
        {
          connection: {
            host: ipv4Address,
            privateKey: node.privateKey,
            user: Cluster.sshUsername,
            perDialTimeout: 10,
            dialErrorLimit: 30,
          },
          create: "echo up",
        },
        { ...emptyOpts, dependsOn: flatcarInstall, ignoreChanges: ["connection", "create"] },
      );
    });
  }

  private fileAssetFromString(content: pulumi.Input<string>): pulumi.Output<pulumi.asset.Asset> {
    return pulumi.all([content]).apply(([content]) => {
      const filename = `/tmp/${crypto.createHash("sha1").update(content).digest("hex")}`;

      // eslint-disable-next-line security/detect-non-literal-fs-filename
      fs.writeFileSync(filename, content, { mode: 0o600 });

      return new pulumi.asset.FileAsset(filename);
    });
  }

  private static ethernetInterfaceName(os: string, serverType: string): { primary: string; private: string[] } {
    let primaryInterface: string;
    let privateInterfaces: string[];

    switch (os) {
      case "fcos":
        primaryInterface = Cluster.interfacesMap.fcos.DEFAULT.primary;
        privateInterfaces = Cluster.interfacesMap.fcos.DEFAULT.private;

        for (const [pattern, iface] of Object.entries(Cluster.interfacesMap.fcos)) {
          if (RegExp(pattern).exec(serverType)) {
            primaryInterface = iface.primary;
            privateInterfaces = iface.private;
          }
        }

        return { primary: primaryInterface, private: privateInterfaces };
      case "flatcar":
        primaryInterface = Cluster.interfacesMap.flatcar.DEFAULT.primary;
        privateInterfaces = Cluster.interfacesMap.flatcar.DEFAULT.private;

        for (const [pattern, iface] of Object.entries(Cluster.interfacesMap.flatcar)) {
          if (RegExp(pattern).exec(serverType)) {
            primaryInterface = iface.primary;
            privateInterfaces = iface.private;
          }
        }

        return { primary: primaryInterface, private: privateInterfaces };
      default:
        throw new Error(`Unsupported operating system ${os}`);
    }
  }
}
