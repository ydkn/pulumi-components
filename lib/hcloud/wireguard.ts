import * as pulumi from "@pulumi/pulumi";
import * as hcloud from "@pulumi/hcloud";
import * as k8s from "@pulumi/kubernetes";
import { ComponentResource } from "../common/index.js";
import * as node from "../node/index.js";
import * as wireguard from "../wireguard/index.js";
import { DnsEndpoint, DnsEndpointsArgs } from "../externalDns/index.js";

export interface WireguardArgs extends wireguard.WireguardArgs {
  nodes: pulumi.Input<pulumi.Input<node.Node>[]>;
  networkId: pulumi.Input<number>;
  hostnames?: pulumi.Input<pulumi.Input<string>[]>;
}

export class Wireguard extends ComponentResource {
  private readonly wireguard: wireguard.Wireguard;
  private readonly firewall: hcloud.Firewall;
  private readonly networkRoutes: pulumi.Output<hcloud.NetworkRoute[]>;
  private readonly dnsEndpoint?: DnsEndpoint;

  /**
   * Create a new wireguard resource.
   * @param name The name of the wireguard resource.
   * @param args A bag of arguments to control the wireguard deployment.
   * @param opts A bag of options that control this resource's behavior.
   */
  constructor(name: string, args: WireguardArgs, opts?: pulumi.ComponentResourceOptions) {
    super("hcloud:wireguard", name, args, opts);

    const hcloudOpts = this.opts(hcloud.Provider);
    const k8sOpts = this.opts(k8s.Provider);

    this.wireguard = new wireguard.Wireguard(name, args, { ...opts, parent: this });

    const gatewayNodes = node.gatewayNodes(args.nodes);

    this.firewall = new hcloud.Firewall(
      name,
      {
        name,
        applyTos: gatewayNodes.apply((nodes) =>
          pulumi.all(nodes).apply((nodes) =>
            nodes.map((node) => {
              return { server: parseInt(node.id!) };
            }),
          ),
        ),
        rules: [
          {
            description: "UDP",
            direction: "in",
            protocol: "udp",
            port: pulumi.interpolate`${this.wireguard.interfacePort}`,
            sourceIps: ["0.0.0.0/0", "::/0"],
          },
        ],
      },
      hcloudOpts,
    );

    this.networkRoutes = pulumi.all([gatewayNodes, args.peers]).apply(([gatewayNodes, peers]) =>
      pulumi.all(gatewayNodes ?? []).apply((gatewayNodes) => {
        const gatewayIPs = gatewayNodes.map((node) => node.privateIpv4Address!);

        if (gatewayIPs.length === 0) {
          return pulumi.output([] as hcloud.NetworkRoute[]);
        }

        return pulumi.all(peers ?? []).apply((peers) => {
          const peerIPRanges: string[] = [];

          for (const peer of peers) {
            for (const ip of peer.allowedIPs) {
              if (/^\d{1,3}\.\d{1,3}\.\d{1,3}\.\d{1,3}\/\d{1,2}$/.exec(ip)) {
                peerIPRanges.push(ip);
              }
            }
          }

          return peerIPRanges.map(
            (peerIPRange) =>
              new hcloud.NetworkRoute(
                `${name}-${peerIPRange}`,
                { networkId: args.networkId, destination: peerIPRange, gateway: gatewayIPs[0] },
                { ...hcloudOpts, deleteBeforeReplace: true },
              ),
          );
        });
      }),
    );

    if (args.hostnames !== undefined) {
      this.dnsEndpoint = new DnsEndpoint(
        name,
        {
          metadata: { namespace: this.wireguard.namespace, name },
          spec: {
            endpoints: pulumi.all([args.hostnames, gatewayNodes]).apply(([hostnames, gatewayNodes]) =>
              pulumi.all(hostnames).apply((hostnames) =>
                pulumi.all(gatewayNodes).apply((nodes) => {
                  const ipv4Endpoints: string[] = [];
                  const ipv6Endpoints: string[] = [];

                  for (const node of nodes) {
                    if (node.ipv4Address !== undefined) {
                      ipv4Endpoints.push(node.ipv4Address);
                    }

                    if (node.ipv6Address !== undefined) {
                      ipv6Endpoints.push(node.ipv6Address);
                    }
                  }

                  const endpoints: DnsEndpointsArgs[] = [];

                  for (const dnsName of hostnames) {
                    if (ipv4Endpoints.length > 0) {
                      endpoints.push({ dnsName, recordType: "A", targets: ipv4Endpoints });
                    }

                    if (ipv6Endpoints.length > 0) {
                      endpoints.push({ dnsName, recordType: "AAAA", targets: ipv6Endpoints });
                    }
                  }

                  return endpoints;
                }),
              ),
            ),
          },
        },
        k8sOpts,
      );
    }

    this.registerOutputs();
  }
}
