import * as pulumi from "@pulumi/pulumi";
import * as k8s from "@pulumi/kubernetes";
import * as fs from "fs";
import * as path from "path";
import * as url from "url";
import { ComponentResource } from "../common/index.js";
import * as kubernetes from "../kubernetes/index.js";
import { Dashboard } from "../grafana/index.js";
import { Certificate } from "../certManager/index.js";

export interface StorageNodeDeviceArg {
  name: pulumi.Input<string>;
}

export interface StorageNodeArg {
  name: pulumi.Input<string>;
  devices: pulumi.Input<pulumi.Input<StorageNodeDeviceArg>[]>;
}

export interface PlacementArgs extends k8s.types.input.core.v1.Affinity {
  tolerations?: pulumi.Input<pulumi.Input<k8s.types.input.core.v1.Toleration>[]>;
  topologySpreadConstraints?: pulumi.Input<pulumi.Input<k8s.types.input.core.v1.TopologySpreadConstraint>[]>;
}

export interface StorageClassDeviceSetsArg {
  name: pulumi.Input<string>;
  count: pulumi.Input<number>;
  preparePlacement?: pulumi.Input<PlacementArgs>;
  placement?: pulumi.Input<PlacementArgs>;
  portable?: pulumi.Input<boolean>;
  encrypted?: pulumi.Input<boolean>;
  volumeClaimTemplates: pulumi.Input<pulumi.Input<k8s.core.v1.PersistentVolumeClaimArgs>[]>;
}

export interface MonitorArgs {
  count?: pulumi.Input<number>;
  volumeClaimTemplate?: pulumi.Input<k8s.core.v1.PersistentVolumeClaimArgs>;
}

export interface ManagerArgs {
  count?: pulumi.Input<number>;
}

export interface DashboardArgs {
  hostname?: pulumi.Input<string>;
  ingressClassName?: pulumi.Input<string>;
  clusterIssuer?: pulumi.Input<string>;
  prometheusEndpoint?: pulumi.Input<string>;
}

export interface ClusterArgs {
  namespace?: pulumi.Input<string>;
  clusterName?: pulumi.Input<string>;
  deviceFilter?: pulumi.Input<string>;
  devicePathFilter?: pulumi.Input<string>;
  storageNodes?: pulumi.Input<pulumi.Input<StorageNodeArg>[]>;
  storageClassDeviceSets?: pulumi.Input<pulumi.Input<StorageClassDeviceSetsArg>[]>;
  mon?: pulumi.Input<MonitorArgs>;
  mgr?: pulumi.Input<ManagerArgs>;
  dashboard?: pulumi.Input<DashboardArgs>;
}

export class Cluster extends ComponentResource {
  private readonly helmChart: kubernetes.helm.v3.Chart;
  private readonly csiCephFsControllerUserSecret: k8s.core.v1.Secret;
  private readonly csiCephFsUserSecret: k8s.core.v1.Secret;
  private readonly grafanaDashboard: Dashboard;
  public readonly namespace: pulumi.Output<string>;
  public readonly csiFSControllerExpandSecretRef: pulumi.Output<k8s.types.input.core.v1.SecretReference>;
  public readonly csiFSNodeStageSecretRef: pulumi.Output<k8s.types.input.core.v1.SecretReference>;

  /**
   * Create a new rook ceph cluster resource.
   * @param name The name of the rook ceph cluster resource.
   * @param args A bag of arguments to control the rook ceph cluster creation.
   * @param opts A bag of options that control this resource's behavior.
   */
  constructor(name: string, args: ClusterArgs, opts?: pulumi.ComponentResourceOptions) {
    super("rook:ceph:cluster", name, args, opts);

    opts = this.opts(k8s.Provider);

    if (args.clusterName === undefined) {
      args.clusterName = "ceph";
    }

    this.namespace = args.namespace
      ? pulumi.output(args.namespace)
      : new k8s.core.v1.Namespace(name, { metadata: { name } }, opts).metadata.name;

    this.helmChart = new kubernetes.helm.v3.Chart(
      name,
      {
        namespace: this.namespace,
        fetchOpts: { repo: "https://charts.rook.io/release" },
        chart: "rook-ceph-cluster",
        values: {
          fullnameOverride: name,
          operatorNamespace: this.namespace,
          clusterName: args.clusterName,
          configOverride: [
            "[global]",
            "mon allow pool delete = true",
            "osd pool default size = 3",
            "osd pool default min size = 2",
            "osd mclock scheduler client wgt = 100",
            "osd mclock scheduler client res = 1",
            "osd mclock scheduler client lim = 999999",
            "osd mclock scheduler background recovery wgt = 10",
            "osd mclock scheduler background recovery res = 0",
            "osd mclock scheduler background recovery lim = 999999",
            "osd mclock scheduler background best effort wgt = 25",
            "osd mclock scheduler background best effort res = 0",
            "osd mclock scheduler background best effort lim = 999999",
          ].join("\n"),
          cephClusterSpec: {
            mon: {
              count: pulumi.all([args.mon]).apply(([mon]) => mon?.count ?? 3),
              allowMultiplePerNode: false,
              volumeClaimTemplate: pulumi.all([args.mon]).apply(([mon]) => mon?.volumeClaimTemplate),
            },
            mgr: {
              count: pulumi.all([args.mgr]).apply(([mgr]) => mgr?.count ?? 2),
            },
            removeOSDsIfOutAndSafeToRemove: true,
            upgradeOSDRequiresHealthyPGs: false,
            continueUpgradeAfterChecksEvenIfNotHealthy: true,
            crashCollector: { disable: true },
            logCollector: { enabled: false },
            storage: {
              useAllDevices: false,
              deviceFilter: args.deviceFilter,
              devicePathFilter: args.devicePathFilter,
              nodes: args.storageNodes,
              storageClassDeviceSets: args.storageClassDeviceSets,
            },
            resources: {
              mgr: { requests: { cpu: "200m", memory: "768Mi" }, limits: { cpu: "4000m", memory: "1792Mi" } },
              mon: { requests: { cpu: "200m", memory: "512Mi" }, limits: { cpu: "4000m", memory: "1536Mi" } },
              osd: { requests: { cpu: "500m", memory: "2560Mi" }, limits: { cpu: "4000m", memory: "4096Mi" } },
              prepareosd: { requests: { cpu: "500m", memory: "256Mi" }, limits: { cpu: "1000m", memory: "1024Mi" } },
              "mgr-sidecar": { requests: { cpu: "200m", memory: "32Mi" }, limits: { cpu: "1000m", memory: "48Mi" } },
              crashcollector: { requests: { cpu: "10m", memory: "32Mi" }, limits: { cpu: "100m", memory: "48Mi" } },
              logcollector: { requests: { cpu: "10m", memory: "32Mi" }, limits: { cpu: "100m", memory: "64Mi" } },
              cleanup: { requests: { cpu: "10m", memory: "32Mi" }, limits: { cpu: "100m", memory: "64Mi" } },
              exporter: { requests: { cpu: "200m", memory: "32Mi" }, limits: { cpu: "1000m", memory: "64Mi" } },
            },
            dashboard: pulumi.all([args.dashboard ?? {}]).apply(([dashboard]) => {
              return {
                enabled: dashboard.hostname !== undefined,
                prometheusEndpoint: dashboard.prometheusEndpoint,
              };
            }),
          },
          ingress: {
            dashboard: pulumi.all([args.dashboard ?? {}]).apply(([dashboard]) => {
              return {
                ingressClassName: dashboard.ingressClassName,
                host: { name: dashboard.hostname },
                annotations: {
                  "traefik.ingress.kubernetes.io/router.entrypoints": "websecure",
                  "traefik.ingress.kubernetes.io/router.tls": "true",
                  "cert-manager.io/cluster-issuer": dashboard.clusterIssuer ?? Certificate.DefaultClusterIssuer,
                },
                tls: [{ secretName: `${name}-ingress-tls`, hosts: [dashboard.hostname] }],
              };
            }),
          },
          monitoring: { enabled: true, createPrometheusRules: false },
          toolbox: { enabled: true },
        },
        transformations: [
          kubernetes.transformations.filter([
            { group: "ceph.rook.io", kind: "CephBlockPool" },
            { group: "ceph.rook.io", kind: "CephFilesystem" },
            { group: "ceph.rook.io", kind: "CephObjectStore" },
            { group: "storage.k8s.io", kind: "StorageClass" },
          ]),
        ],
      },
      {},
      opts,
    );

    const csiController = this.namespace.apply((namespace) =>
      k8s.core.v1.Secret.get(`${name}-csi-cephfs-provisioner`, `${namespace}/rook-csi-cephfs-provisioner`, {
        ...opts,
        dependsOn: [this.helmChart],
      }),
    );

    this.csiCephFsControllerUserSecret = new k8s.core.v1.Secret(
      `${name}-csi-cephfs-provisioner-user`,
      {
        metadata: { name: pulumi.interpolate`${csiController.metadata.name}-user`, namespace: this.namespace },
        stringData: {
          adminID: csiController.data.adminID.apply((v) => Buffer.from(v, "base64").toString("ascii")),
          adminKey: csiController.data.adminKey.apply((v) => Buffer.from(v, "base64").toString("ascii")),
          userID: csiController.data.adminID.apply((v) => Buffer.from(v, "base64").toString("ascii")),
          userKey: csiController.data.adminKey.apply((v) => Buffer.from(v, "base64").toString("ascii")),
        },
      },
      opts,
    );

    this.csiFSControllerExpandSecretRef = pulumi.output({
      name: this.csiCephFsControllerUserSecret.metadata.name,
      namespace: this.csiCephFsControllerUserSecret.metadata.namespace,
    });

    const csiCephfsNode = this.namespace.apply((namespace) =>
      k8s.core.v1.Secret.get(`${name}-csi-cephfs-node`, `${namespace}/rook-csi-cephfs-node`, {
        ...opts,
        dependsOn: [this.helmChart],
      }),
    );

    this.csiCephFsUserSecret = new k8s.core.v1.Secret(
      `${name}-csi-cephfs-node-user`,
      {
        metadata: { name: pulumi.interpolate`${csiCephfsNode.metadata.name}-user`, namespace: this.namespace },
        stringData: {
          adminID: csiCephfsNode.data.adminID.apply((v) => Buffer.from(v, "base64").toString("ascii")),
          adminKey: csiCephfsNode.data.adminKey.apply((v) => Buffer.from(v, "base64").toString("ascii")),
          userID: csiCephfsNode.data.adminID.apply((v) => Buffer.from(v, "base64").toString("ascii")),
          userKey: csiCephfsNode.data.adminKey.apply((v) => Buffer.from(v, "base64").toString("ascii")),
        },
      },
      opts,
    );

    this.csiFSNodeStageSecretRef = pulumi.output({
      name: this.csiCephFsUserSecret.metadata.name,
      namespace: this.csiCephFsUserSecret.metadata.namespace,
    });

    this.grafanaDashboard = new Dashboard(
      `${name}-grafana-dashboard`,
      {
        metadata: { namespace: this.namespace, name: pulumi.interpolate`${name}-grafana-dashboard` },
        name: "rook-ceph",
        data: fs.readFileSync(
          path.join(path.dirname(url.fileURLToPath(import.meta.url)), "assets", "dashboard.json"),
          "utf-8",
        ),
      },
      opts,
    );

    this.registerOutputs();
  }
}
