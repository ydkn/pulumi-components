import * as pulumi from "@pulumi/pulumi";
import * as k8s from "@pulumi/kubernetes";

export interface ProviderSpecificArgs {
  name: pulumi.Input<string>;
  value: pulumi.Input<string>;
}

export interface DnsEndpointsArgs {
  dnsName: pulumi.Input<string>;
  targets: pulumi.Input<pulumi.Input<string>[]>;
  recordType: pulumi.Input<string>;
  recordTTL?: pulumi.Input<number>;
  labels?: pulumi.Input<{ [key: string]: pulumi.Input<string> }>;
  providerSpecific?: pulumi.Input<pulumi.Input<ProviderSpecificArgs>[]>;
}

export interface DnsEndpointSpecArgs {
  endpoints: pulumi.Input<pulumi.Input<DnsEndpointsArgs>[]>;
}

export interface DnsEndpointArgs {
  metadata: pulumi.Input<k8s.types.input.meta.v1.ObjectMeta>;
  spec: pulumi.Input<DnsEndpointSpecArgs>;
}

export class DnsEndpoint extends k8s.apiextensions.CustomResource {
  /**
   * Create a new DNS endpoint resource.
   * @param name The name of the DNS endpoint resource.
   * @param args A bag of arguments to control the DNS endpoint creation.
   * @param opts A bag of options that control this resource's behavior.
   */
  constructor(name: string, args: DnsEndpointArgs, opts?: pulumi.CustomResourceOptions) {
    super(name, { ...args, apiVersion: "externaldns.k8s.io/v1alpha1", kind: "DNSEndpoint" }, opts);
  }
}
