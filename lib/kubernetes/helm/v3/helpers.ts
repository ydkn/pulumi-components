import * as pulumi from "@pulumi/pulumi";
import { Certificate } from "../../../certManager/index.js";

export function ingressValues(
  name: pulumi.Input<string>,
  hostnames?: pulumi.Input<pulumi.Input<string | undefined>[]>,
  ingressClassName?: pulumi.Input<string>,
): pulumi.Output<{
  enabled: boolean;
  ingressClassName?: string;
  hosts?: string[];
  pathType?: string;
  tls?: { secretName: string; hosts: string[] }[];
  annotations?: { [key: string]: string };
}> {
  return pulumi.all([name, hostnames, ingressClassName]).apply(([name, hostnames, ingressClassName]) => {
    if (hostnames === undefined) {
      return { enabled: false };
    }

    const hosts = hostnames.filter((h) => h !== undefined);

    if (hosts.length === 0) {
      return { enabled: false };
    }

    return {
      enabled: true,
      ingressClassName,
      hosts,
      pathType: "ImplementationSpecific",
      tls: [{ secretName: `${name}-ingress-tls`, hosts }],
      annotations: {
        "traefik.ingress.kubernetes.io/router.entrypoints": "websecure",
        "traefik.ingress.kubernetes.io/router.tls": "true",
        "cert-manager.io/cluster-issuer": Certificate.DefaultClusterIssuer,
      },
    };
  });
}
