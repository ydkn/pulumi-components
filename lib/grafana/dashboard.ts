import * as pulumi from "@pulumi/pulumi";
import * as k8s from "@pulumi/kubernetes";

export interface DashboardArgs {
  metadata: pulumi.Input<k8s.types.input.meta.v1.ObjectMeta>;
  name: pulumi.Input<string>;
  data: pulumi.Input<string>;
}

export class Dashboard extends k8s.core.v1.ConfigMap {
  /**
   * Create a new Grafana dashboard resource.
   * @param name The name of the Grafana dashboard resource.
   * @param args A bag of arguments to control the Grafana dashboard creation.
   * @param opts A bag of options that control this resource's behavior.
   */
  constructor(name: string, args: DashboardArgs, opts?: pulumi.CustomResourceOptions) {
    super(
      name,
      {
        metadata: pulumi.all([args.metadata]).apply(([metadata]) => {
          return {
            ...metadata,
            labels: { ...metadata.labels, grafana_dashboard: "1" },
          };
        }),
        data: pulumi.all([args.name, args.data]).apply(([dashboardName, data]) => {
          const d: { [key: string]: string } = {};

          d[`${dashboardName}.json`] = data;

          return d;
        }),
      },
      opts,
    );
  }
}
