import * as pulumi from "@pulumi/pulumi";
import * as k8s from "@pulumi/kubernetes";
import { RelabelingArgs, TLSConfigArgs } from "./monitor.js";

export interface NamespaceSelector {
  matchNames?: pulumi.Input<pulumi.Input<string | undefined>[]>;
}

export interface EndpointArgs {
  path?: pulumi.Input<string>;
  port?: pulumi.Input<string | number>;
  honorLabels?: pulumi.Input<boolean>;
  honorTimestamps?: pulumi.Input<boolean>;
  interval?: pulumi.Input<string>;
  scrapeTimeout?: pulumi.Input<string>;
  bearerTokenFile?: pulumi.Input<string>;
  scheme?: pulumi.Input<string>;
  tlsConfig?: pulumi.Input<TLSConfigArgs>;
  metricRelabelings?: pulumi.Input<pulumi.Input<RelabelingArgs>[]>;
  relabelings?: pulumi.Input<pulumi.Input<RelabelingArgs>[]>;
}

export interface ServiceMonitorSpec {
  namespaceSelector?: pulumi.Input<NamespaceSelector>;
  selector?: pulumi.Input<k8s.types.input.meta.v1.LabelSelector>;
  endpoints?: pulumi.Input<pulumi.Input<EndpointArgs>[]>;
}

export interface ServiceMonitorArgs {
  metadata: pulumi.Input<k8s.types.input.meta.v1.ObjectMeta>;
  spec: pulumi.Input<ServiceMonitorSpec>;
}

export class ServiceMonitor extends k8s.apiextensions.CustomResource {
  /**
   * Create a new service monitor resource.
   * @param name The name of the service monitor resource.
   * @param args A bag of arguments to control the service monitor creation.
   * @param opts A bag of options that control this resource's behavior.
   */
  constructor(name: string, args: ServiceMonitorArgs, opts?: pulumi.CustomResourceOptions) {
    super(name, { ...args, apiVersion: "monitoring.coreos.com/v1", kind: "ServiceMonitor" }, opts);
  }
}
