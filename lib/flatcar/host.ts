import * as pulumi from "@pulumi/pulumi";
import * as tls from "@pulumi/tls";
import { ComponentResource } from "../common/index.js";
import { IgnitionUserArgs } from "../node/index.js";
import { ConfigArgs, Config } from "./config.js";

export interface HostUserArgs {
  name: pulumi.Input<string>;
  groups?: pulumi.Input<pulumi.Input<string>[]>;
}

export interface HostArgs extends ConfigArgs {
  sshUsers?: pulumi.Input<pulumi.Input<HostUserArgs>[]>;
}

export class Host extends ComponentResource {
  private static readonly defaultModules: string[] = ["wireguard"];

  public readonly sshPrivateKeys: pulumi.Output<{ [key: string]: tls.PrivateKey }>;
  public readonly ignitionConfig: pulumi.Output<string>;

  /**
   * Create a new Flatcar host resource.
   * @param name The name of the Flatcar host resource.
   * @param args A bag of arguments to control the Flatcar host creation.
   * @param opts A bag of options that control this resource's behavior.
   */
  constructor(name: string, args: HostArgs, opts?: pulumi.ComponentResourceOptions) {
    super("flatcar:host", name, args, opts);

    const emptyOpts = this.opts();

    this.sshPrivateKeys = pulumi.all([args.sshUsers]).apply(([sshUsers]) => {
      const sshPrivateKeys: { [key: string]: tls.PrivateKey } = {};

      for (const sshUser of sshUsers ?? []) {
        sshPrivateKeys[sshUser.name] = new tls.PrivateKey(
          `${name}-${sshUser.name}`,
          { algorithm: "ED25519" },
          emptyOpts,
        );
      }

      return sshPrivateKeys;
    });

    this.ignitionConfig = new Config(
      name,
      {
        ...args,
        files: pulumi.all([args.files ?? []]).apply(([files]) =>
          files.concat(
            Host.defaultModules.map((module) => ({
              path: `/etc/modules-load.d/${module}.conf`,
              contents: module,
              mode: 0o644,
            })),
          ),
        ),
        users: pulumi
          .all([args.users, args.sshUsers, this.sshPrivateKeys])
          .apply(([users, sshUsers, sshPrivateKeys]) => {
            const allUsers: pulumi.Input<IgnitionUserArgs>[] = [];

            for (const user of users ?? []) {
              allUsers.push(user);
            }

            for (const sshUser of sshUsers ?? []) {
              allUsers.push({
                name: sshUser.name,
                groups: sshUser.groups,
                sshAuthorizedKeys: sshPrivateKeys[sshUser.name].publicKeyOpenssh.apply((key) => [key.trim()]),
              });
            }

            return allUsers;
          }),
        systemdUnits: [],
      },
      emptyOpts,
    ).ignitionConfig;

    this.registerOutputs();
  }
}
