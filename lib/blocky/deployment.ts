import * as pulumi from "@pulumi/pulumi";
import * as k8s from "@pulumi/kubernetes";
import * as fs from "fs";
import * as path from "path";
import * as url from "url";
import * as yaml from "js-yaml";
import { ComponentResource } from "../common/index.js";
import * as kubernetes from "../kubernetes/index.js";
import { ServiceMonitor } from "../prometheus/index.js";
import { Dashboard } from "../grafana/index.js";

export interface RedisArgs {
  address: pulumi.Input<string>;
  database?: pulumi.Input<number>;
}

export interface BlockyArgs {
  namespace?: pulumi.Input<string>;
  nameservers?: pulumi.Input<pulumi.Input<string>[]>;
  blocklists?: pulumi.Input<{ [key: string]: pulumi.Input<pulumi.Input<string>[]> }>;
  redis?: pulumi.Input<RedisArgs>;
  hostname?: pulumi.Input<string>;
  serviceType?: pulumi.Input<string>;
  serviceLabels?: pulumi.Input<{ [key: string]: pulumi.Input<string> }>;
  serviceLoadBalancerClass?: pulumi.Input<string>;
  replicas?: pulumi.Input<number>;
}

export class Blocky extends ComponentResource {
  private readonly config: k8s.core.v1.ConfigMap;
  private readonly deployment: k8s.apps.v1.Deployment;
  private readonly service: kubernetes.v1.Service;
  private readonly metricService: kubernetes.v1.Service;
  private readonly serviceMonitor: ServiceMonitor;
  private readonly grafanaDashboard: Dashboard;
  public readonly namespace: pulumi.Output<string>;

  /**
   * Create a new Blocky resource.
   * @param name The name of the Blocky resource.
   * @param args A bag of arguments to control the Blocky creation.
   * @param opts A bag of options that control this resource's behavior.
   */
  constructor(name: string, args: BlockyArgs, opts?: pulumi.ComponentResourceOptions) {
    super("blocky", name, args, opts);

    const k8sOpts = this.opts(k8s.Provider);

    if (args.replicas === undefined) {
      args.replicas = 1;
    }

    this.namespace = args.namespace
      ? pulumi.output(args.namespace)
      : new k8s.core.v1.Namespace(name, { metadata: { name } }, k8sOpts).metadata.name;

    const metadata = {
      name,
      namespace: this.namespace,
      labels: {
        "app.kubernetes.io/name": "blocky",
        "app.kubernetes.io/instance": name,
      },
    };

    const upstreamServers = args.nameservers ?? ["193.110.81.0", "185.253.5.0"];

    this.config = new k8s.core.v1.ConfigMap(
      name,
      {
        metadata,
        data: {
          "config.yml": pulumi.all([args.blocklists, args.redis]).apply(([blocklists, redis]) =>
            yaml.dump({
              logLevel: "warn",
              logFormat: "json",
              logPrivacy: true,
              port: 53,
              tlsPort: 853,
              httpPort: 4000,
              httpsPort: 4443,
              upstream: { default: upstreamServers },
              blocking: {
                blockType: "nxDomain",
                blockTTL: "6h",
                downloadAttempts: 3,
                downloadCooldown: "2s",
                downloadTimeout: "60s",
                failStartOnListError: false,
                processingConcurrency: 4,
                refreshPeriod: "4h",
                blackLists: blocklists,
                clientGroupsBlock: { default: Object.keys(blocklists ?? []) },
              },
              caching: { prefetching: true },
              redis: redis
                ? {
                    address: redis.address,
                    database: redis.database ?? 0,
                    connectionAttempts: 10,
                    connectionCooldown: "3s",
                    required: true,
                  }
                : undefined,
              prometheus: { enable: true, path: "/metrics" },
            }),
          ),
        },
      },
      k8sOpts,
    );

    this.deployment = new k8s.apps.v1.Deployment(
      name,
      {
        metadata,
        spec: {
          selector: { matchLabels: metadata.labels },
          replicas: args.replicas,
          template: {
            metadata: { labels: metadata.labels },
            spec: {
              dnsPolicy: "ClusterFirst",
              dnsConfig: { options: [{ name: "ndots", value: "1" }] },
              securityContext: {
                runAsUser: 568,
                runAsGroup: 568,
                fsGroup: 568,
                fsGroupChangePolicy: "OnRootMismatch",
                supplementalGroups: [568],
              },
              containers: [
                {
                  name: "blocky",
                  image: "spx01/blocky:latest",
                  imagePullPolicy: "Always",
                  securityContext: { readOnlyRootFilesystem: true, runAsNonRoot: true },
                  volumeMounts: [
                    { name: "config", mountPath: "/app/config.yml", subPath: "config.yml", readOnly: true },
                  ],
                  ports: [
                    { name: "dnstcp", protocol: "TCP", containerPort: 53 },
                    { name: "dnsudp", protocol: "UDP", containerPort: 53 },
                    { name: "dot", protocol: "TCP", containerPort: 853 },
                    { name: "http", protocol: "TCP", containerPort: 4000 },
                    { name: "https", protocol: "TCP", containerPort: 4443 },
                  ],
                  resources: {
                    requests: { memory: "96Mi" },
                    limits: { memory: "128Mi" },
                  },
                },
              ],
              volumes: [{ name: "config", configMap: { name: this.config.metadata.name, defaultMode: 420 } }],
              affinity: {
                podAntiAffinity: {
                  preferredDuringSchedulingIgnoredDuringExecution: [
                    {
                      podAffinityTerm: {
                        labelSelector: {
                          matchExpressions: [
                            {
                              key: "app.kubernetes.io/name",
                              operator: "In",
                              values: [metadata.labels["app.kubernetes.io/name"]],
                            },
                            {
                              key: "app.kubernetes.io/instance",
                              operator: "In",
                              values: [metadata.labels["app.kubernetes.io/instance"]],
                            },
                          ],
                        },
                        topologyKey: "kubernetes.io/hostname",
                      },
                      weight: 100,
                    },
                  ],
                },
              },
            },
          },
        },
      },
      k8sOpts,
    );

    this.service = new kubernetes.v1.Service(
      name,
      {
        metadata: {
          ...metadata,
          labels: pulumi.all([args.serviceLabels]).apply(([labels]) => {
            return { ...metadata.labels, ...labels };
          }),
        },
        spec: {
          loadBalancerClass: args.serviceLoadBalancerClass,
          type: args.serviceType ?? "ClusterIP",
          selector: metadata.labels,
          ports: [
            { name: "udp", protocol: "UDP", port: 53 },
            { name: "tcp", protocol: "TCP", port: 53 },
            { name: "dot", protocol: "TCP", port: 853 },
          ],
        },
        hostnames: args.hostname ? [args.hostname] : undefined,
      },
      { ...k8sOpts, dependsOn: [this.deployment] },
    );

    const metricsMetadata = {
      ...metadata,
      name: `${name}-metrics`,
      labels: { ...metadata.labels, "app.kubernetes.io/component": "metrics" },
    };

    this.metricService = new kubernetes.v1.Service(
      `${name}-metrics`,
      {
        metadata: metricsMetadata,
        spec: {
          selector: metadata.labels,
          ports: [{ name: "http", protocol: "TCP", port: 4000 }],
        },
      },
      { ...k8sOpts, dependsOn: [this.deployment] },
    );

    this.serviceMonitor = new ServiceMonitor(
      `${name}-metrics`,
      {
        metadata: metricsMetadata,
        spec: {
          namespaceSelector: { matchNames: [this.namespace] },
          selector: { matchLabels: metricsMetadata.labels },
          endpoints: [{ port: "http", path: "/metrics", interval: "1m", scrapeTimeout: "30s" }],
        },
      },
      k8sOpts,
    );

    this.grafanaDashboard = new Dashboard(
      `${name}-grafana-dashboard`,
      {
        metadata: { ...metadata, name: pulumi.interpolate`${name}-grafana-dashboard` },
        name: "blocky",
        data: fs.readFileSync(
          path.join(path.dirname(url.fileURLToPath(import.meta.url)), "assets", "dashboard.json"),
          "utf-8",
        ),
      },
      k8sOpts,
    );

    this.registerOutputs();
  }
}
