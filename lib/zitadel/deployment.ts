import * as pulumi from "@pulumi/pulumi";
import * as random from "@pulumi/random";
import * as k8s from "@pulumi/kubernetes";
import * as mailgun from "@pulumi/mailgun";
import { ComponentResource } from "../common/index.js";
import * as kubernetes from "../kubernetes/index.js";

export interface DatabaseArgs {
  hostname: pulumi.Input<string>;
  port?: pulumi.Input<number>;
  database?: pulumi.Input<string>;
  secret: pulumi.Input<string>;
}

export interface MailgunArgs {
  domain: pulumi.Input<string>;
  region?: pulumi.Input<string>;
  smtpHost: pulumi.Input<string>;
}

export interface ZitadelArgs {
  namespace?: pulumi.Input<string>;
  hostname: pulumi.Input<string>;
  ingressClassName?: pulumi.Input<string>;
  clusterIssuer?: pulumi.Input<string>;
  database: pulumi.Input<DatabaseArgs>;
  mailgun: pulumi.Input<MailgunArgs>;
  replicas?: pulumi.Input<number>;
}

export class Zitadel extends ComponentResource {
  private readonly smtpCredentials: mailgun.DomainCredential;
  private readonly masterKey: pulumi.Output<string>;
  private readonly masterKeySecret: kubernetes.v1.Secret;
  private readonly helmRelease: kubernetes.helm.v3.Release;
  public readonly namespace: pulumi.Output<string>;

  /**
   * Create a new Zitadel resource.
   * @param name The name of the Zitadel resource.
   * @param args A bag of arguments to control the Zitadel creation.
   * @param opts A bag of options that control this resource's behavior.
   */
  constructor(name: string, args: ZitadelArgs, opts?: pulumi.ComponentResourceOptions) {
    super("zitadel", name, args, opts);

    const emptyOpts = this.opts();
    const k8sOpts = this.opts(k8s.Provider);
    const mailgunOpts = this.opts(mailgun.Provider);

    if (args.replicas === undefined) {
      args.replicas = 1;
    }

    const smtpPassword = new random.RandomPassword(`${name}-smtp-password`, { length: 32 }, emptyOpts).result;

    this.smtpCredentials = new mailgun.DomainCredential(
      name,
      {
        region: pulumi.all([args.mailgun]).apply(([mailgun]) => mailgun.region ?? "eu"),
        domain: pulumi.all([args.mailgun]).apply(([mailgun]) => mailgun.domain),
        login: pulumi.interpolate`zitadel-${name}`,
        password: smtpPassword,
      },
      mailgunOpts,
    );

    this.namespace = args.namespace
      ? pulumi.output(args.namespace)
      : new k8s.core.v1.Namespace(name, { metadata: { name } }, k8sOpts).metadata.name;

    const metadata = {
      name,
      namespace: this.namespace,
      labels: {
        "app.kubernetes.io/name": "zitadel",
        "app.kubernetes.io/component": "zitadel",
        "app.kubernetes.io/instance": name,
      },
    };

    this.masterKey = pulumi.secret(
      new random.RandomPassword(`${name}-master-key`, { length: 32, special: false }, emptyOpts).result,
    );

    this.masterKeySecret = new kubernetes.v1.Secret(
      `${name}-master-key`,
      {
        metadata: { ...metadata, name: `${name}-master-key` },
        stringData: {
          masterkey: this.masterKey,
        },
      },
      k8sOpts,
    );

    const databaseSecret = pulumi.all([this.namespace, args.database]).apply(([namespace, database]) => {
      let secretNamespace = namespace;
      let secretName = database.secret;

      const secret = secretName.split("/", 2);
      if (secret.length === 2) {
        secretNamespace = secret[0];
        secretName = secret[1];
      }

      return k8s.core.v1.Secret.get(`${name}-postgres`, `${secretNamespace}/${secretName}`, k8sOpts);
    });

    this.helmRelease = new kubernetes.helm.v3.Release(
      name,
      {
        name,
        namespace: this.namespace,
        repositoryOpts: { repo: "https://charts.zitadel.com" },
        chart: "zitadel",
        values: {
          fullnameOverride: name,
          replicaCount: args.replicas,
          zitadel: {
            masterkeySecretName: this.masterKeySecret.metadata.name,
            secretConfig: {
              Database: {
                cockroach: { Host: "" },
                postgres: {
                  Host: pulumi.all([args.database]).apply(([database]) => database.hostname),
                  Port: pulumi.all([args.database]).apply(([database]) => database.port ?? 5432),
                  Database: pulumi.all([args.database]).apply(([database]) => database.database ?? "zitadel"),
                  Admin: {
                    Username: databaseSecret.data.username.apply((v) => Buffer.from(v, "base64").toString("ascii")),
                    Password: databaseSecret.data.password.apply((v) => Buffer.from(v, "base64").toString("ascii")),
                    SSL: { Mode: "require" },
                  },
                  User: {
                    Username: databaseSecret.data.username.apply((v) => Buffer.from(v, "base64").toString("ascii")),
                    Password: databaseSecret.data.password.apply((v) => Buffer.from(v, "base64").toString("ascii")),
                    SSL: { Mode: "require" },
                  },
                },
              },
              DefaultInstance: {
                SMTPConfiguration: {
                  SMTP: {
                    Host: pulumi.all([args.mailgun]).apply(([mailgun]) => pulumi.interpolate`${mailgun.smtpHost}:587`),
                    User: pulumi
                      .all([args.mailgun, this.smtpCredentials.login])
                      .apply(([mailgun, login]) => pulumi.interpolate`${login}@${mailgun.domain}`),
                    Password: smtpPassword,
                  },
                  TLS: true,
                  FromName: "Zitadel",
                  From: pulumi.interpolate`${name}@${args.hostname}`,
                  ReplyToAddress: pulumi.interpolate`${name}@${args.hostname}`,
                },
              },
              TLS: { Enabled: false },
              ExternalDomain: args.hostname,
              ExternalPort: 443,
              ExternalSecure: true,
              Telemetry: { Enabled: false },
            },
          },
          service: {
            annotations: {
              "traefik.ingress.kubernetes.io/service.serversscheme": "h2c",
              "traefik.ingress.kubernetes.io/service.passhostheader": "true",
            },
          },
          ingress: {
            enabled: true,
            className: args.ingressClassName,
            annotations: {
              "traefik.ingress.kubernetes.io/router.entrypoints": "websecure",
              "traefik.ingress.kubernetes.io/router.tls": "true",
              "cert-manager.io/cluster-issuer": args.clusterIssuer,
            },
            hosts: [{ host: args.hostname, paths: [{ path: "/", pathType: "Prefix" }] }],
            tls: [{ hosts: [args.hostname], secretName: `${name}-ingress-tls` }],
          },
          metrics: {
            enabled: true,
            serviceMonitor: { enabled: true },
          },
          pdb: { enabled: true, minAvailable: 1 },
          affinity: {
            podAntiAffinity: {
              preferredDuringSchedulingIgnoredDuringExecution: [
                {
                  weight: 100,
                  podAffinityTerm: {
                    labelSelector: {
                      matchExpressions: [
                        {
                          key: "app.kubernetes.io/name",
                          operator: "In",
                          values: [metadata.labels["app.kubernetes.io/name"]],
                        },
                        {
                          key: "app.kubernetes.io/instance",
                          operator: "In",
                          values: [metadata.labels["app.kubernetes.io/instance"]],
                        },
                      ],
                    },
                    topologyKey: "kubernetes.io/hostname",
                  },
                },
              ],
            },
          },
          resources: {
            requests: { memory: "640Mi" },
            limits: { memory: "896Mi" },
          },
        },
      },
      k8sOpts,
    );

    this.registerOutputs();
  }
}
