import * as pulumi from "@pulumi/pulumi";
import * as k8s from "@pulumi/kubernetes";
import * as transformations from "../../transformations/index.js";

export interface ChartTransformers {
  deprecations?: boolean;
  metadataNamespace?: boolean;
  metadataNamespaceName?: pulumi.Input<string>;
}

export interface ChartArgs {
  transformers?: ChartTransformers;
  skipCRDs?: boolean;
}

export class Chart extends k8s.helm.v3.Chart {
  /**
   * Create an instance of the specified Helm chart.
   * @param releaseName Name of the Chart (e.g., nginx-ingress).
   * @param config Configuration options for the Chart.
   * @param args A bag of arguments to control the Chart creation.
   * @param opts A bag of options that control this resource's behavior.
   */
  constructor(
    releaseName: string,
    config: k8s.helm.v3.ChartOpts | k8s.helm.v3.LocalChartOpts,
    args?: ChartArgs,
    opts?: pulumi.ComponentResourceOptions,
  ) {
    if (config.transformations === undefined) {
      config.transformations = [];
    }

    if (args !== undefined) {
      if (args.transformers !== undefined) {
        if (args.transformers.deprecations) {
          config.transformations.push(transformations.v1beta1Deprecations);
        }

        if (args.transformers.metadataNamespace && args.transformers.metadataNamespaceName !== undefined) {
          config.transformations.push(transformations.metadataNamespace(args.transformers.metadataNamespaceName));
        }
      }

      if (args.skipCRDs) {
        config.transformations.push((o: any, _: pulumi.CustomResourceOptions): void => {
          if (o === undefined) {
            return;
          }

          if (o.apiVersion === "apiextensions.k8s.io/v1beta1" || o.apiVersion === "apiextensions.k8s.io/v1") {
            if (o.kind === "CustomResourceDefinition") {
              o.apiVersion = "v1";
              o.kind = "List";
              o.items = [];
            }
          }
        });
      }
    }

    super(releaseName, config, opts);
  }
}
