import * as pulumi from "@pulumi/pulumi";
import * as k8s from "@pulumi/kubernetes";
import * as fs from "fs";
import * as path from "path";
import * as url from "url";
import { ComponentResource } from "../common/index.js";
import * as kubernetes from "../kubernetes/index.js";

export interface AgentArgs {
  namespace?: pulumi.Input<string>;
  managementUrl: pulumi.Input<string>;
  setupKey: pulumi.Input<string>;
  version?: pulumi.Input<string>;
  interfaceName?: pulumi.Input<string>;
  interfacePort?: pulumi.Input<number>;
  networkRange?: pulumi.Input<string>;
  masqueradeAll?: pulumi.Input<boolean>;
  mss?: pulumi.Input<number>;
  nodeSelector?: pulumi.Input<{ [key: string]: pulumi.Input<string> }>;
  extraBlacklistInterfaces?: pulumi.Input<pulumi.Input<string>[]>;
}

export class Agent extends ComponentResource {
  private static readonly extraBlacklistInterfaces = [
    "wireguard.cali",
    "wg-v6.cali",
    "tunl0",
    "lxc_health",
    "cilium_host",
    "cilium_net",
    "cilium_vxlan",
  ];
  public static readonly interfaceName = "wt0";
  public static readonly interfacePort = 51820;

  private readonly extraBlacklistInterfaces: pulumi.Output<pulumi.Output<string>[]>;
  private readonly secret: kubernetes.v1.Secret;
  private readonly nftablesScript: k8s.core.v1.ConfigMap;
  private readonly daemonSet: k8s.apps.v1.DaemonSet;
  private readonly podDisruptionBudget: k8s.policy.v1.PodDisruptionBudget;
  public readonly namespace: pulumi.Output<string>;
  public readonly interfaceName: pulumi.Output<string>;
  public readonly interfacePort: pulumi.Output<number>;

  /**
   * Create a new Netbird agent resource.
   * @param name The name of the Netbird agent resource.
   * @param args A bag of arguments to control the Netbird agent creation.
   * @param opts A bag of options that control this resource's behavior.
   */
  constructor(name: string, args: AgentArgs, opts?: pulumi.ComponentResourceOptions) {
    super("netbird:agent", name, args, opts);

    opts = this.opts(k8s.Provider);

    this.interfaceName = args.interfaceName ? pulumi.output(args.interfaceName) : pulumi.output(Agent.interfaceName);
    this.interfacePort = args.interfacePort ? pulumi.output(args.interfacePort) : pulumi.output(Agent.interfacePort);
    this.extraBlacklistInterfaces = pulumi
      .all([args.extraBlacklistInterfaces, this.interfaceName])
      .apply(([extraBlacklistInterfaces, interfaceName]) =>
        pulumi.all(extraBlacklistInterfaces ?? []).apply((extraBlacklistInterfaces) => {
          const interfaces = Agent.extraBlacklistInterfaces;

          interfaces.push(interfaceName);

          for (const iface of extraBlacklistInterfaces) {
            if (!interfaces.includes(iface)) {
              interfaces.push(iface);
            }
          }

          return interfaces.map((iface) => pulumi.output(iface));
        }),
      );

    this.namespace = args.namespace
      ? pulumi.output(args.namespace)
      : new k8s.core.v1.Namespace(name, { metadata: { name } }, opts).metadata.name;

    const metadata = {
      name,
      namespace: this.namespace,
      labels: {
        "app.kubernetes.io/name": "netbird",
        "app.kubernetes.io/component": "agent",
        "app.kubernetes.io/instance": name,
      },
    };

    const secretName = name;
    const nftablesScriptName = `${metadata.name}-nftables`;

    this.secret = new kubernetes.v1.Secret(
      name,
      {
        metadata: { ...metadata, name: secretName },
        stringData: {
          SETUP_KEY: args.setupKey,
          ADMIN_URL: args.managementUrl,
          MANAGEMENT_URL: args.managementUrl,
          INTERFACE_NAME: this.interfaceName,
          WIREGUARD_PORT: pulumi.interpolate`${this.interfacePort}`,
          LOG_LEVEL: "info",
          USE_LEGACY_ROUTING: "true",
        },
      },
      opts,
    );

    this.nftablesScript = new k8s.core.v1.ConfigMap(
      name,
      {
        metadata: { ...metadata, name: nftablesScriptName },
        data: {
          "nftables.sh": fs.readFileSync(
            path.join(path.dirname(url.fileURLToPath(import.meta.url)), "assets", "nftables.sh"),
            "utf-8",
          ),
        },
      },
      opts,
    );

    this.daemonSet = new k8s.apps.v1.DaemonSet(
      name,
      {
        metadata,
        spec: {
          selector: { matchLabels: metadata.labels },
          template: {
            metadata: { labels: metadata.labels },
            spec: {
              tolerations: [
                { key: "node-role.kubernetes.io/master", operator: "Exists", effect: "NoSchedule" },
                { key: "node-role.kubernetes.io/control-plane", operator: "Exists", effect: "NoSchedule" },
              ],
              nodeSelector: args.nodeSelector,
              hostNetwork: true,
              containers: [
                {
                  name: "netbird",
                  image: pulumi.interpolate`netbirdio/netbird:${args.version ?? "latest"}`,
                  imagePullPolicy: "Always",
                  securityContext: { capabilities: { add: ["NET_ADMIN", "SYS_RESOURCE", "SYS_ADMIN"] } },
                  args: [
                    "--extra-iface-blacklist",
                    this.extraBlacklistInterfaces.apply((interfaces) =>
                      pulumi.all(interfaces).apply((interfaces) => interfaces.join(",")),
                    ),
                  ],
                  envFrom: [{ secretRef: { name: secretName }, prefix: "NB_" }],
                  ports: [
                    {
                      name: "wireguard",
                      protocol: "UDP",
                      containerPort: this.interfacePort,
                      hostPort: this.interfacePort,
                    },
                  ],
                  volumeMounts: [{ name: "config", mountPath: "/etc/netbird/" }],
                  resources: {
                    requests: { memory: "48Mi" },
                    limits: { memory: "64Mi" },
                  },
                },
                {
                  name: "nftables",
                  image: "alpine:latest",
                  imagePullPolicy: "Always",
                  securityContext: { capabilities: { add: ["NET_ADMIN"] } },
                  command: ["/usr/local/bin/nftables.sh"],
                  envFrom: [{ secretRef: { name: secretName }, prefix: "NB_" }],
                  env: [
                    { name: "NETWORK_RANGE", value: args.networkRange ?? "" },
                    {
                      name: "MASQUERADE_ALL",
                      value: pulumi
                        .all([args.masqueradeAll])
                        .apply(([masqueradeAll]) => (masqueradeAll ? "true" : "false")),
                    },
                    { name: "MSS", value: pulumi.all([args.mss]).apply(([mss]) => (mss ? String(mss) : "")) },
                  ],
                  volumeMounts: [{ name: "nftables", mountPath: "/usr/local/bin" }],
                  resources: {
                    requests: { memory: "16Mi" },
                    limits: { memory: "96Mi" },
                  },
                },
              ],
              volumes: [
                { name: "config", hostPath: { path: `/var/lib/netbird/${name}`, type: "DirectoryOrCreate" } },
                { name: "nftables", configMap: { name: nftablesScriptName, defaultMode: 0o755 } },
              ],
              terminationGracePeriodSeconds: 10,
            },
          },
        },
      },
      opts,
    );

    this.podDisruptionBudget = new k8s.policy.v1.PodDisruptionBudget(
      name,
      {
        metadata,
        spec: {
          minAvailable: 1,
          selector: { matchLabels: metadata.labels },
        },
      },
      opts,
    );

    this.registerOutputs();
  }
}
