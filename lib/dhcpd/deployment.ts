import * as pulumi from "@pulumi/pulumi";
import * as k8s from "@pulumi/kubernetes";
import { ComponentResource } from "../common/index.js";

export interface Subnet {
  subnet: string;
  nameservers: string[];
  start: string;
  end: string;
  netmask: string;
  router: string;
}

export interface DhcpdArgs {
  namespace?: pulumi.Input<string>;
  subnets: pulumi.Input<pulumi.Input<Subnet>[]>;
  nodeSelector?: pulumi.Input<{ [key: string]: pulumi.Input<string> }>;
}

export class Dhcpd extends ComponentResource {
  private readonly config: k8s.core.v1.ConfigMap;
  private readonly deployment: k8s.apps.v1.Deployment;

  /**
   * Create a new DHCPd resource.
   * @param name The name of the DHCPd resource.
   * @param args A bag of arguments to control the DHCPd deployment.
   * @param opts A bag of options that control this resource's behavior.
   */
  constructor(name: string, args: DhcpdArgs, opts?: pulumi.ComponentResourceOptions) {
    super("dhcpd", name, args, opts);

    opts = this.opts(k8s.Provider);

    let namespace = args.namespace;

    if (args.namespace === undefined) {
      namespace = new k8s.core.v1.Namespace(name, { metadata: { name } }, opts).metadata.name;
    }

    const metadata = {
      name,
      namespace,
      labels: {
        "app.kubernetes.io/name": "dhcpd",
        "app.kubernetes.io/component": "dhcpd",
        "app.kubernetes.io/instance": name,
      },
    };

    this.config = new k8s.core.v1.ConfigMap(
      name,
      {
        metadata,
        data: {
          "dhcpd.conf": pulumi.all([args.subnets]).apply(([subnets]) => {
            const confOptions = ["authoritative;", "ddns-update-style none;"];

            for (const s of subnets) {
              confOptions.push(
                `subnet ${s.subnet} netmask ${s.netmask} {`,
                `range ${s.start} ${s.end};`,
                `option domain-name-servers ${s.nameservers.join(", ")};`,
                `option routers ${s.router};`,
                "default-lease-time 3600;",
                "max-lease-time 86400;",
                "}",
              );
            }

            return confOptions.join("\n");
          }),
        },
      },
      opts,
    );

    this.deployment = new k8s.apps.v1.Deployment(
      name,
      {
        metadata,
        spec: {
          selector: { matchLabels: metadata.labels },
          strategy: { type: "Recreate" },
          replicas: 1,
          template: {
            metadata: { labels: metadata.labels },
            spec: {
              nodeSelector: args.nodeSelector,
              tolerations: [
                { effect: "NoSchedule", operator: "Exists" },
                { effect: "NoExecute", operator: "Exists" },
              ],
              hostNetwork: true,
              initContainers: [
                {
                  name: "settings",
                  image: "busybox:latest",
                  command: ["/bin/sh"],
                  args: ["-c", "cat /config/dhcpd.conf > /data/dhcpd.conf"],
                  volumeMounts: [
                    { name: "config", mountPath: "/config" },
                    { name: "data", mountPath: "/data" },
                  ],
                  resources: {
                    requests: { memory: "8Mi", cpu: "100m" },
                    limits: { memory: "16Mi", cpu: "200m" },
                  },
                },
              ],
              containers: [
                {
                  name: "dhcpd",
                  image: "networkboot/dhcpd:latest",
                  securityContext: {
                    privileged: true,
                    capabilities: { add: ["NET_ADMIN"] },
                  },
                  volumeMounts: [{ name: "data", mountPath: "/data" }],
                  resources: {
                    requests: { memory: "16Mi", cpu: "100m" },
                    limits: { memory: "32Mi", cpu: "400m" },
                  },
                },
              ],
              volumes: [
                { name: "config", configMap: { name: this.config.metadata.name } },
                { name: "data", emptyDir: {} },
              ],
              affinity: {
                podAntiAffinity: {
                  preferredDuringSchedulingIgnoredDuringExecution: [
                    {
                      weight: 100,
                      podAffinityTerm: {
                        labelSelector: {
                          matchExpressions: [
                            {
                              key: "app.kubernetes.io/name",
                              operator: "In",
                              values: [metadata.labels["app.kubernetes.io/name"]],
                            },
                            {
                              key: "app.kubernetes.io/instance",
                              operator: "In",
                              values: [metadata.labels["app.kubernetes.io/instance"]],
                            },
                          ],
                        },
                        topologyKey: "kubernetes.io/hostname",
                      },
                    },
                  ],
                },
              },
            },
          },
        },
      },
      opts,
    );

    this.registerOutputs();
  }
}
