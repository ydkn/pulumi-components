import * as pulumi from "@pulumi/pulumi";
import * as random from "@pulumi/random";
import * as k8s from "@pulumi/kubernetes";
import * as aws from "@pulumi/aws";
import { ComponentResource } from "../common/index.js";
import * as kubernetes from "../kubernetes/index.js";
import { Bucket } from "../aws/index.js";

export interface VeleroArgs {
  namespace?: pulumi.Input<string>;
  clusterName: pulumi.Input<string>;
  protect?: boolean;
}

export class Velero extends ComponentResource {
  private readonly repoPasswordSecret: kubernetes.v1.Secret;
  private readonly helmRelease: kubernetes.helm.v3.Release;
  private readonly bucket: Bucket;
  public readonly resticPassword: pulumi.Output<string>;
  public readonly namespace: pulumi.Output<string>;

  /**
   * Create a new Velero resource.
   * @param name The name of the Velero resource.
   * @param args A bag of arguments to control the Velero creation.
   * @param opts A bag of options that control this resource's behavior.
   */
  constructor(name: string, args: VeleroArgs, opts?: pulumi.ComponentResourceOptions) {
    super("velero", name, {}, opts);

    const emptyOpts = this.opts();
    const awsOpts = this.opts(aws.Provider);
    const k8sOpts = this.opts(k8s.Provider);

    this.bucket = new Bucket(
      name,
      { name: pulumi.interpolate`${name}-${args.clusterName}`, protect: args.protect },
      awsOpts,
    );

    this.resticPassword = pulumi.secret(
      new random.RandomPassword(name, { length: 64, special: false }, emptyOpts).result,
    );

    this.namespace = args.namespace
      ? pulumi.output(args.namespace)
      : new k8s.core.v1.Namespace(name, { metadata: { name } }, k8sOpts).metadata.name;

    const metadata = {
      name,
      namespace: this.namespace,
      labels: {
        "app.kubernetes.io/name": "velero",
        "app.kubernetes.io/component": "velero",
        "app.kubernetes.io/instance": name,
      },
    };

    this.repoPasswordSecret = new kubernetes.v1.Secret(
      name,
      {
        metadata: { ...metadata, name: pulumi.interpolate`${name}-repo-credentials` },
        stringData: {
          "repository-password": this.resticPassword,
        },
      },
      k8sOpts,
    );

    this.helmRelease = new kubernetes.helm.v3.Release(
      name,
      {
        name: metadata.name,
        namespace: metadata.namespace,
        repositoryOpts: { repo: "https://vmware-tanzu.github.io/helm-charts" },
        chart: "velero",
        values: {
          fullnameOverride: name,
          backupsEnabled: true,
          snapshotsEnabled: false,
          deployNodeAgent: true,
          configuration: {
            backupStorageLocation: [
              {
                name: "default",
                provider: "aws",
                bucket: this.bucket.bucket.id,
                config: {
                  region: this.bucket.bucket.region,
                },
              },
            ],
          },
          credentials: {
            secretContents: {
              cloud: pulumi
                .all([this.bucket.accessKey.accessKey.id, this.bucket.accessKey.accessKey.secret])
                .apply(([accessKey, secretKey]) =>
                  ["[default]", `aws_access_key_id=${accessKey}`, `aws_secret_access_key=${secretKey}`].join("\n"),
                ),
            },
          },
          nodeAgent: {
            privileged: true,
            resources: {
              requests: { memory: "192Mi" },
              limits: { memory: "384Mi" },
            },
            tolerations: [
              { key: "node-role.kubernetes.io/master", operator: "Exists", effect: "NoSchedule" },
              { key: "node-role.kubernetes.io/control-plane", operator: "Exists", effect: "NoSchedule" },
            ],
          },
          initContainers: [
            {
              name: "velero-plugin-for-aws",
              image: "velero/velero-plugin-for-aws:latest",
              volumeMounts: [{ name: "plugins", mountPath: "/target" }],
              resources: {
                requests: { memory: "32Mi" },
                limits: { memory: "48Mi" },
              },
            },
          ],
          schedules: {
            daily: {
              schedule: "0 2 * * *",
              template: {
                ttl: "170h", // ~7 days
              },
            },
            weekly: {
              schedule: "0 3 * * 0",
              template: {
                ttl: "730h", // ~4 weeks
              },
            },
            monthly: {
              schedule: "0 4 1 * *",
              template: {
                ttl: "2200h", // ~3 months
              },
            },
          },
          affinity: {
            podAntiAffinity: {
              preferredDuringSchedulingIgnoredDuringExecution: [
                {
                  weight: 100,
                  podAffinityTerm: {
                    labelSelector: {
                      matchExpressions: [
                        { key: "app.kubernetes.io/name", operator: "In", values: ["velero"] },
                        { key: "app.kubernetes.io/instance", operator: "In", values: [name] },
                      ],
                    },
                    topologyKey: "kubernetes.io/hostname",
                  },
                },
              ],
            },
          },
          resources: {
            requests: { memory: "256Mi" },
            limits: { memory: "512Mi" },
          },
          metrics: { serviceMonitor: { enabled: true } },
        },
      },
      k8sOpts,
    );

    this.registerOutputs();
  }
}
